# -*- coding: iso-8859-1 -*-
theApp.EvtMax = vars().get('EVTMAX', -1)
from AthenaCommon.Logging import logging
myLog = logging.getLogger('p4JobOptions')

from AthenaCommon.GlobalFlags import globalflags as globalFlags
globalFlags.DataSource.set_Value_and_Lock("data")
globalFlags.InputFormat.set_Value_and_Lock("bytestream")

from RecExConfig.RecFlags import rec
rec.projectName.set_Value_and_Lock('data09')

import ByteStreamCnvSvc.ReadByteStream

region = "Tile"
run_no = 289656
year = "16"

svcMgr.ByteStreamInputSvc.FullFileName = [
"/afs/cern.ch/work/c/cantel/private/PHOS4/"+region+"/data"+year+"_calib.00"+str(run_no)+".calibration_L1CaloPprPhos4ScanPars.daq.RAW._lb0000._SFO-1._0001.data"
]

from AthenaCommon.AthenaCommonFlags  import athenaCommonFlags
athenaCommonFlags.FilesInput = svcMgr.ByteStreamInputSvc.FullFileName


myLog.info('------------------------------> 1')
include('TrigT1CaloByteStream/ReadLVL1CaloBSRun1_jobOptions.py')
myLog.info('------------------------------> 2')

svcMgr.IOVDbSvc.GlobalTag = "COMCOND-ES1C-000-00"

#Define output file name and stream name
rootStreamName = "MyHiststream"
rootFileName   = "/afs/cern.ch/work/c/cantel/private/output/"+str(run_no)+"/recon_P4pulses.root"


svcMgr += CfgMgr.THistSvc()
svcMgr.THistSvc.Output += ["%s DATAFILE='%s' OPT='RECREATE'" % (rootStreamName, rootFileName)]

from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()
topSequence += CfgMgr.xAODMaker__EventInfoCnvAlg()
topSequence += CfgMgr.xAODMaker__RODHeaderCnvAlg ()
#opSequence += CfgMgr.reconstructp4("phos4recon", OutputLevel=DEBUG)
topSequence += CfgMgr.reconstructp4("phos4recon", run=run_no, OutputLevel=DEBUG, RootStreamName=rootStreamName, RootDirName="/Hists")
