# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* reconstruct phos4 pulses
* do fitting to derive fitting parameters for each trigger tower.


### How do I get set up? ###

* athena version 20.1.3.3 (asetup 20.1.3.3,here)

### To run ###

#### reconstruct Phos4 pulses ####
* run with athena using job options file p4JobOptions.py.
* job options are "Tile/LAr", "run number" and "15/16" (year).
(note: need to first create directory <run number> in output directory..should be fixed).
* **outputs** a root file 'recon_p4pulses.root' and xml file 'TTproperties_run<run number>'  in <run number> directory.

#### run fitting procedure ####

* load run_fitpulse.cxx in root (root -l), need to also load (.L) TTxml.cxx, FitFunctions.cxx.
* run fitPulserRun('<run number>') (other arguments are given defaults)
* **outputs** root files 'reffit_hists_new.root', 'GLufit_hists_new.root', 'LLufit_hists_new.root' and 'fitquality_hists_new.root'
* modifies or outputs xml file 'TTparameters.xml' in base output directory (creates new if does not exist yet, else modifies by updating parameters if better chi2/ndf value).
     

