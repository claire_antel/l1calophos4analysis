#include "TTxml.h"
#include <map>
#include "TH2TT.h"

#include "TFile.h"

#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <stdio.h>
#include <algorithm>

void drawmaps( std::string filledORunfilled) {
      
	const char * outputfile_maps;
	const char * input_TTfile;
	
	if (filledORunfilled == "filled") {
	  outputfile_maps="/afs/cern.ch/work/c/cantel/private/output/maps_filledblanks.root"; 
	  input_TTfile="/afs/cern.ch/work/c/cantel/private/output/TTparameters_filledblanks.xml";
	}
	else if (filledORunfilled == "unfilled") {
	  outputfile_maps="/afs/cern.ch/work/c/cantel/private/output/maps_unfilledblanks.root"; 
	  input_TTfile="/afs/cern.ch/work/c/cantel/private/output/TTparameters.xml";
	}
	else { std::cerr<<"Run option given not valid - choose between 'filled' or 'unfilled'."<<std::endl; return; }
	
	TFile output_maps(outputfile_maps,"RECREATE");
	//TFile comparison_maps("/afs/cern.ch/work/c/cantel/private/output/comparisons_map.root","RECREATE");

	// for input
	TTxml m_par;
	TTxml m_par_runI;
	TTxml fixed_pars;
	std::map <unsigned int, double> sigmal_map;
	std::map <unsigned int, double> sigmar_map;
	std::map <unsigned int, double> sigmal_runI_map;
	std::map <unsigned int, double> sigmar_runI_map;
	std::map <unsigned int, double> sigmal_diff_map;
	std::map <unsigned int, double> sigmar_diff_map;
	std::map <unsigned int, double> us_map;
	std::map <unsigned int, double> DA_map;
	std::map <unsigned int, double> chi2ndf_map;
	std::map <unsigned int, double> func_map;
	std::map <unsigned int, double> ped_map;



	ifstream ifile0(input_TTfile);
	ifstream ifile1("/afs/cern.ch/work/c/cantel/private/output/runI/input_Overlap.xml");
	if (ifile0 && ifile1) {
		m_par.Read(input_TTfile);
		sigmal_map = m_par.Get("sigmal");
		sigmar_map = m_par.Get("sigmar");
		us_map = m_par.Get("us");
		DA_map = m_par.Get("undVsAmpl");
		chi2ndf_map = m_par.Get("chi2ndf");
		func_map = m_par.Get("func");
		ped_map = m_par.Get("pedestal");

		m_par_runI.Read("/afs/cern.ch/work/c/cantel/private/output/runI/input_Overlap.xml");
		sigmal_runI_map = m_par_runI.Get("sigmaLeft_new");
		sigmar_runI_map = m_par_runI.Get("sigmaRight_new");
	}
	else puts ("TT parameter file does not exist");


	TTxml m_properties;
        std::map<unsigned int, double> eta_map;
        std::map<unsigned int, double> phi_map;
        std::map<unsigned int, double> layer_map;
        std::map<unsigned int, double> part_map;

	ifstream ifile2("/afs/cern.ch/work/c/cantel/private/output/264603/TTproperties_run264603.xml");
	if (ifile2) {
		m_properties.Read("/afs/cern.ch/work/c/cantel/private/output/264603/TTproperties_run264603.xml");
		eta_map = m_properties.Get("etas");
		phi_map = m_properties.Get("phis");
		layer_map = m_properties.Get("layers");
		part_map = m_properties.Get("parts");
	}
	else puts ("TT parameter file does not exist");

	
        TH2TT *emsigmal = new TH2TT("emsigmal","sigma_l (EM)");
        emsigmal->GetXaxis()->SetTitle("eta");
        emsigmal->GetYaxis()->SetTitle("phi");
        emsigmal->Fillall(-1000);
        emsigmal->SetOption("colz");
        TH2TT *emsigmar = new TH2TT("emsigmar","sigma_r (EM)");
        emsigmar->GetXaxis()->SetTitle("eta");
        emsigmar->GetYaxis()->SetTitle("phi");
        emsigmar->Fillall(-1000);
        emsigmar->SetOption("colz");
        TH2TT *hadsigmal = new TH2TT("hadsigmal","sigma_l (HAD)");
        hadsigmal->GetXaxis()->SetTitle("eta");
        hadsigmal->GetYaxis()->SetTitle("phi");
        hadsigmal->Fillall(-1000);
        hadsigmal->SetOption("colz");
        TH2TT *hadsigmar = new TH2TT("hadsigmar","sigma_r (HAD)");
        hadsigmar->GetXaxis()->SetTitle("eta");
        hadsigmar->GetYaxis()->SetTitle("phi");
        hadsigmar->Fillall(-1000);
        hadsigmar->SetOption("colz");
        TH2TT *emus = new TH2TT("emus","undershoot (EM)");
        emus->GetXaxis()->SetTitle("eta");
        emus->GetYaxis()->SetTitle("phi");
        emus->Fillall(-1000);
        emus->SetOption("colz");
        TH2TT *hadus = new TH2TT("hadus","undershoot (HAD)");
        hadus->GetXaxis()->SetTitle("eta");
        hadus->GetYaxis()->SetTitle("phi");
        hadus->Fillall(-1000);
        hadus->SetOption("colz");
        TH2TT *emDA = new TH2TT("emDA","undershoot/A (EM)");
        emDA->GetXaxis()->SetTitle("eta");
        emDA->GetYaxis()->SetTitle("phi");
        emDA->Fillall(-1000);
        emDA->SetOption("colz");
        TH2TT *hadDA = new TH2TT("hadDA","undershoot/A (HAD)");
        hadDA->GetXaxis()->SetTitle("eta");
        hadDA->GetYaxis()->SetTitle("phi");
        hadDA->Fillall(-1000);
        hadDA->SetOption("colz");
        TH2TT *emchi2ndf = new TH2TT("emchi2ndf","chi2/ndf (EM)");
        emchi2ndf->GetXaxis()->SetTitle("eta");
        emchi2ndf->GetYaxis()->SetTitle("phi");
        emchi2ndf->Fillall(-1000);
        emchi2ndf->SetOption("colz");
        TH2TT *hadchi2ndf = new TH2TT("hadchi2ndf","chi2/ndf (HAD)");
        hadchi2ndf->GetXaxis()->SetTitle("eta");
        hadchi2ndf->GetYaxis()->SetTitle("phi");
        hadchi2ndf->Fillall(-1000);
        hadchi2ndf->SetOption("colz");
        TH2TT *emfunc = new TH2TT("emfunc","function used (1: GLu, 2: LLu) (EM)");
        emfunc->GetXaxis()->SetTitle("eta");
        emfunc->GetYaxis()->SetTitle("phi");
        emfunc->Fillall(-1000);
        emfunc->SetOption("colz");
        TH2TT *hadfunc = new TH2TT("hadfunc","function used (1: GLu, 2: LLu) (HAD)");
        hadfunc->GetXaxis()->SetTitle("eta");
        hadfunc->GetYaxis()->SetTitle("phi");
        hadfunc->Fillall(-1000);
        hadfunc->SetOption("colz");
        TH2TT *emped = new TH2TT("emped","pedestal value (EM)");
        emped->GetXaxis()->SetTitle("eta");
        emped->GetYaxis()->SetTitle("phi");
        emped->Fillall(-1000);
        emped->SetOption("colz");
        TH2TT *hadped = new TH2TT("hadped","pedestal value (HAD)");
        hadped->GetXaxis()->SetTitle("eta");
        hadped->GetYaxis()->SetTitle("phi");
        hadped->Fillall(-1000);
        hadped->SetOption("colz");

        TH2TT *emsigmal_diff = new TH2TT("emsigmal_diff","sigma_l RunII - sigma_l RunI  (EM)");
        emsigmal_diff->GetXaxis()->SetTitle("eta");
        emsigmal_diff->GetYaxis()->SetTitle("phi");
        emsigmal_diff->Fillall(-1000);
        emsigmal_diff->SetOption("colz");
        TH2TT *emsigmar_diff = new TH2TT("emsigmar_diff","sigma_r RunII - sigma_r RunI(EM)");
        emsigmar_diff->GetXaxis()->SetTitle("eta");
        emsigmar_diff->GetYaxis()->SetTitle("phi");
        emsigmar_diff->Fillall(-1000);
        emsigmar_diff->SetOption("colz");
        TH2TT *hadsigmal_diff = new TH2TT("hadsigmal_diff","sigma_lRunII - sigma_l RunI (HAD)");
        hadsigmal_diff->GetXaxis()->SetTitle("eta");
        hadsigmal_diff->GetYaxis()->SetTitle("phi");
        hadsigmal_diff->Fillall(-1000);
        hadsigmal_diff->SetOption("colz");
        TH2TT *hadsigmar_diff = new TH2TT("hadsigmar_diff","sigma_r RunII - sigma_r RunI (HAD)");
        hadsigmar_diff->GetXaxis()->SetTitle("eta");
        hadsigmar_diff->GetYaxis()->SetTitle("phi");
        hadsigmar_diff->Fillall(-1000);
        hadsigmar_diff->SetOption("colz");

        for (std::map<unsigned int, double>::iterator i =sigmal_map.begin(); i!=sigmal_map.end(); i++){ 

		unsigned int CoolID = i->first;
		double eta = (double) eta_map[CoolID];
		double phi = (double) phi_map[CoolID];
		double part = (double) part_map[CoolID];
		int layer = (int) layer_map[CoolID];
		double sigmal = sigmal_map[CoolID];
		double sigmar = sigmar_map[CoolID];
		double sigmal_runI = (double) sigmal_runI_map[CoolID];
		double sigmar_runI = (double) sigmar_runI_map[CoolID];
		double us = us_map[CoolID];
		double DA = DA_map[CoolID];
		double chi2ndf = chi2ndf_map[CoolID];
		double func = func_map[CoolID];
		double ped = ped_map[CoolID];

		//std::cout<<"cool ID is... "<<CoolID<<std::endl;
		std::cout<<"sigmal run I is... "<<sigmal_runI<<std::endl;
		//std::cout<<"sigmar run I is... "<<sigmar_runI<<std::endl;

		if (layer == 0){
			emsigmal->SetBinContent(eta, phi, sigmal);
			emsigmar->SetBinContent(eta, phi, sigmar);
			emus->SetBinContent(eta, phi, us);
			emDA->SetBinContent(eta, phi, DA);
			emchi2ndf->SetBinContent(eta, phi, chi2ndf);
			emfunc->SetBinContent(eta, phi, func);
			emped->SetBinContent(eta, phi, ped);

			double diffl_em = sigmal-sigmal_runI;
			double diffr_em = sigmar-sigmar_runI;
			emsigmal_diff->SetBinContent(eta, phi, diffl_em);
			emsigmar_diff->SetBinContent(eta, phi, diffr_em);

			sigmal_diff_map[CoolID]=diffl_em;
			sigmar_diff_map[CoolID]=diffr_em;

			if (fabs(diffl_em)>1.) std::cout<<"part: "<<part<<", diffl: "<<diffl_em<<", diffr: "<<diffr_em<<" , ID: "<<std::hex<<CoolID<< std::endl;
			if (fabs(diffr_em)>1.) std::cout<<"part: "<<part<<", diffl: "<<diffl_em<<", diffr: "<<diffr_em<<" , ID: "<<std::hex<<CoolID<< std::endl;
		}
		else {
			hadsigmal->SetBinContent(eta, phi, sigmal);
			hadsigmar->SetBinContent(eta, phi, sigmar);
			hadus->SetBinContent(eta, phi, us);
			hadDA->SetBinContent(eta, phi, DA);
			hadchi2ndf->SetBinContent(eta, phi, chi2ndf);
			hadfunc->SetBinContent(eta, phi, func);
			hadped->SetBinContent(eta, phi, ped);
			
			//std::cout<<"diff is... "<<sigmal-sigmal_runI<<std::endl;
			double diffl_had = sigmal-sigmal_runI;
			double diffr_had = sigmar-sigmar_runI;
			hadsigmal_diff->SetBinContent(eta, phi, diffl_had );
			hadsigmar_diff->SetBinContent(eta, phi, diffr_had );

			sigmal_diff_map[CoolID]=diffl_had;
			sigmar_diff_map[CoolID]=diffr_had;

			if (fabs(diffl_had)>1.) std::cout<<"part: "<<part<<", diffl: "<<diffl_had<<", diffr: "<<diffr_had<<" , ID: "<<std::hex<<CoolID<< std::endl;
			if (fabs(diffr_had)>1.) std::cout<<"part: "<<part<<", diffl: "<<diffl_had<<", diffr: "<<diffr_had<<" , ID: "<<std::hex<<CoolID<< std::endl;
		}
		

	}

	fixed_pars.Add("sigmal_diff", sigmal_diff_map);	
	fixed_pars.Add("sigmar_diff", sigmar_diff_map);
	fixed_pars.Add("sigmal", sigmal_map);
	fixed_pars.Add("sigmar", sigmar_map);

	ifstream ifile_fixed("/afs/cern.ch/work/c/cantel/private/output/TT_fixedpars.xml");

	if (ifile_fixed) {
		if( remove( "/afs/cern.ch/work/c/cantel/private/output/TT_fixedpars.xml" ) != 0 )
		perror( "Error deleting file" );
  		else puts( "Overwriting TT_fixedpars file." );
	}
	else puts("TT_fixedpars file does not yet exist. Creating new file.");

  	fixed_pars.Write("/afs/cern.ch/work/c/cantel/private/output/TT_fixedpars.xml");	


	emsigmal->SetMinimum(13.);
	emsigmar->SetMinimum(11.8);
	emus->SetMinimum(0.);
	emDA->SetMinimum(0.);
	emchi2ndf->SetMinimum(0.);
	emfunc->SetMinimum(0.);
	hadsigmal->SetMinimum(13.5);
	hadsigmar->SetMinimum(12.);
	hadus->SetMinimum(0.);
	hadDA->SetMinimum(0.);
	hadchi2ndf->SetMinimum(0.);
	hadfunc->SetMinimum(0.);
	emped->SetMinimum(0.);
	hadped->SetMinimum(0.);

	emsigmal_diff->SetMinimum(-5);
	emsigmar_diff->SetMinimum(-5);
	hadsigmal_diff->SetMinimum(-5);
	hadsigmar_diff->SetMinimum(-5);

	emsigmal->SetStats(0);
	emsigmar->SetStats(0);
	emus->SetStats(0);
	emDA->SetStats(0);
	emchi2ndf->SetStats(0);
	emfunc->SetStats(0);
	hadsigmal->SetStats(0);
	hadsigmar->SetStats(0);
	hadus->SetStats(0);
	hadDA->SetStats(0);
	hadchi2ndf->SetStats(0);
	hadfunc->SetStats(0);
	emped->SetStats(0);
	hadped->SetStats(0);

	emsigmal_diff->SetStats(0);
	emsigmar_diff->SetStats(0);
	hadsigmal_diff->SetStats(0);
	hadsigmar_diff->SetStats(0);

	emsigmal->SetMaximum(29.);
	emsigmar->SetMaximum(45.5);
	hadsigmal->SetMaximum(32.);
	hadsigmar->SetMaximum(33.);
		

	emsigmal->Write();
	emsigmar->Write();
	emus->Write();
	emDA->Write();
	emchi2ndf->Write();
	emfunc->Write();
	hadsigmal->Write();
	hadsigmar->Write();
	hadus->Write();
	hadDA->Write();
	hadchi2ndf->Write();
	hadfunc->Write();
	emped->Write();	
	hadped->Write();	

	emsigmal_diff->Write();
	emsigmar_diff->Write();
	hadsigmal_diff->Write();
	hadsigmar_diff->Write();
	
	output_maps.Close();

}


void fillblanks(const char * inputfile_maps="/afs/cern.ch/work/c/cantel/private/output/maps_unfilledblanks.root"){

	TTxml m_par;

	std::map <unsigned int, double> sigmal_map;
	std::map <unsigned int, double> sigmar_map;
	std::map <unsigned int, double> us_map;
	std::map <unsigned int, double> DA_map;
	std::map <unsigned int, double> chi2ndf_map;
	std::map <unsigned int, double> func_map;
	std::map <unsigned int, double> ped_map;

	ifstream ifile0("/afs/cern.ch/work/c/cantel/private/output/TTparameters.xml");
	if (ifile0) {
		m_par.Read("/afs/cern.ch/work/c/cantel/private/output/TTparameters.xml");
		sigmal_map = m_par.Get("sigmal");
		sigmar_map = m_par.Get("sigmar");
		us_map = m_par.Get("us");
		DA_map = m_par.Get("undVsAmpl");
		chi2ndf_map = m_par.Get("chi2ndf");
		func_map = m_par.Get("func");
		ped_map = m_par.Get("pedestal");	
	}
	else puts ("TT parameter file does not exist");

	puts ("TT parameter file opened successfully... continuing...");

	TTxml m_properties;
        std::map<unsigned int, double> eta_map;
        std::map<unsigned int, double> phi_map;
        std::map<unsigned int, double> layer_map;
        std::map<unsigned int, double> part_map;

	ifstream ifile1("/afs/cern.ch/work/c/cantel/private/output/264603/TTproperties_run264603.xml");
	if (ifile1) {
		puts ("TT property file exists");
		m_properties.Read("/afs/cern.ch/work/c/cantel/private/output/264603/TTproperties_run264603.xml");
		eta_map = m_properties.Get("etas");
		phi_map = m_properties.Get("phis");
		layer_map = m_properties.Get("layers");
		part_map = m_properties.Get("parts");
	}
	else puts ("TT parameter file does not exist");

	TH2D *emsigmal;
	TH2D *emsigmar;
	TH2D *hadsigmal;
	TH2D *hadsigmar;
	TH2D *emDA;
	TH2D *hadDA;

	TFile input_maps(inputfile_maps, "READ");

	input_maps.cd();

	gDirectory->GetListOfKeys()->Print();

	gDirectory->GetObject("emsigmal",emsigmal);
	gDirectory->GetObject("emsigmar",emsigmar);
	gDirectory->GetObject("hadsigmal",hadsigmal);
	gDirectory->GetObject("hadsigmar",hadsigmar);
	gDirectory->GetObject("emDA",emDA);
	gDirectory->GetObject("hadDA",hadDA);


	for (std::map<unsigned int, double>::iterator k =eta_map.begin(); k!=eta_map.end(); k++){ 

		unsigned int CoolID = k->first;


		double phi = (double) phi_map[CoolID];
		double eta = (double) eta_map[CoolID];
		int layer = (int) layer_map[CoolID];	

		if (!(sigmal_map.count(CoolID))){
			puts ("COOLID...");
			std::cout<<std::hex<<CoolID<<std::dec<<std::endl;

			unsigned int binx0=emsigmal->GetXaxis()->FindBin(eta);
			unsigned int biny0=emsigmal->GetYaxis()->FindBin(phi);

			if (layer==0){
			  
/*				unsigned int mirror_binx(0);
				if (binx0>33) mirror_binx = 34-(binx0-33);
				else mirror_binx = 67-binx0;*/
			  

				double mirror_emsigmal =  emsigmal->GetBinContent(67-binx0, biny0);
				if (mirror_emsigmal==-1000) {std::cerr<<"mirror tower also invalid! ...stopping."<<std::endl; return;}

				double mirror_emsigmar =  emsigmar->GetBinContent(67-binx0, biny0);
				if (mirror_emsigmar==-1000) {std::cerr<<"mirror tower also invalid! ...stopping."<<std::endl; return;}

				double mirror_emuvsa =  emDA->GetBinContent(67-binx0, biny0);
				if (mirror_emuvsa==-1000) {std::cerr<<"mirror tower also invalid! ...stopping."<<std::endl; return;}				

				sigmal_map[CoolID]=mirror_emsigmal;	
				sigmar_map[CoolID]=mirror_emsigmar;
				DA_map[CoolID]=mirror_emuvsa;
			}
			else {

				double mirror_hadsigmal =  hadsigmal->GetBinContent(67-binx0, biny0);
				if (mirror_hadsigmal==-1000) {std::cerr<<"mirror tower also invalid! ...stopping."<<std::endl; return;}

				double mirror_hadsigmar =  hadsigmar->GetBinContent(67-binx0, biny0);
				if (mirror_hadsigmar==-1000) {std::cerr<<"mirror tower also invalid! ...stopping."<<std::endl; return;}

				double mirror_haduvsa =  hadDA->GetBinContent(67-binx0, biny0);
				if (mirror_haduvsa==-1000) {std::cerr<<"mirror tower also invalid! ...stopping."<<std::endl; return;}				

				sigmal_map[CoolID]=mirror_hadsigmal;	
				sigmar_map[CoolID]=mirror_hadsigmar;
				DA_map[CoolID]=mirror_haduvsa;
			}			
		}
	}

	TTxml m_par_replace;

  	m_par_replace.Add("sigmal", sigmal_map);
  	m_par_replace.Add("sigmar", sigmar_map);
  	m_par_replace.Add("us", us_map);
  	m_par_replace.Add("undVsAmpl", DA_map);
  	m_par_replace.Add("chi2ndf", chi2ndf_map);
  	m_par_replace.Add("func", func_map);
  	m_par_replace.Add("part", part_map);
  	m_par_replace.Add("eta", eta_map);
  	m_par_replace.Add("phi", phi_map);
  	m_par_replace.Add("pedestal", ped_map);

	ifstream ifileB("/afs/cern.ch/work/c/cantel/private/output/TTparameters_filledblanks.xml");

	if (ifileB) {
		if( remove( "/afs/cern.ch/work/c/cantel/private/output/TTparameters_filledblanks.xml" ) != 0 )
		perror( "Error deleting file" );
  		else puts( "File successfully deleted" );
	}
	else puts("TTpar_filled file does not yet exist. Creating new file.");
	
  	m_par_replace.Write("/afs/cern.ch/work/c/cantel/private/output/TTparameters_filledblanks.xml");


}
