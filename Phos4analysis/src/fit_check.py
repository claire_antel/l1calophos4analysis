# check reconstructed pulses and fit.
# output fitted pulse, chi square, sigma values, comparison to run I

from ROOT import *
#gROOT.LoadMacro("TTxml.cxx")

#run = raw_input('look at run: ')

region = raw_input('look at region: ')
TTid = raw_input('look at TT tower: ')
option = raw_input('Compare to Run I (0) or second TT (1)? ')

if option == '1':
	TTid2 = raw_input('look at second TT tower: ')

#run1='264603'#'264505'#'266851'#'264603'
#run2='264604'
#run3='174155'#'175951' #'174155'
#run4='177385'#'176209' #'177385'

if region == "3":
	run1='264603'#'266854'#'264603'#'264505'#'266851'#'264603'
	run2='273449'#'266851'#'264604'	
	run3='174155'
	run4='177385'
	f1 = TFile('/afs/cern.ch/work/c/cantel/private/output/'+run1+'/GLufit_hists_new.root')#TFile('../output/'+run1+'/recon_P4pulses.root')#TFile('../output/'+run1+'/GLufit_hists.root')
	f2 = TFile('/afs/cern.ch/work/c/cantel/private/output/'+run2+'/GLufit_hists_new.root')#TFile('../output/'+run2+'/recon_P4pulses.root')#TFile('../output/'+run2+'/GLufit_hists.root')
	f3 = TFile('/afs/cern.ch/work/c/cantel/private/output/runI/Phos4scan'+run3+'_pedestalcorrected.root')
	f4 = TFile('/afs/cern.ch/work/c/cantel/private/output/runI/Phos4scan'+run4+'_pedestalcorrected.root')
	key="HistfitGLu0x"+TTid+"_success"
	if option == '1':
		key2="HistfitGLu0x"+TTid2+"_success"
elif region in ["7", "8"]:
	run1='264505'
	run2='0'
	run3='175951'
	run4='176209'
	f1 = TFile('/afs/cern.ch/work/c/cantel/private/output/'+run1+'/GLufit_hists_new.root')#TFile('../output/'+run1+'/recon_P4pulses.root')#TFile('../output/'+run1+'/GLufit_hists.root')
	f3 = TFile('/afs/cern.ch/work/c/cantel/private/output/runI/Phos4scan'+run3+'_pedestalcorrected.root')
	f4 = TFile('/afs/cern.ch/work/c/cantel/private/output/runI/Phos4scan'+run4+'_pedestalcorrected.root')
	key="HistfitGLu0x"+TTid+"_success"
	if option == '1':
		key2="HistfitGLu0x"+TTid2+"_success"
else:
	run1="273450" #'264603'#'264505'#'266851'#'264603'
	run2="273449"#'264604'
	run3='174155'
	run4='177385'
	f1 = TFile('/afs/cern.ch/work/c/cantel/private/output/'+run2+'/LLufit_hists_new.root')#TFile('../output/'+run1+'/recon_P4pulses.root')#TFile('../output/'+run1+'/GLufit_hists.root')
	f2 = TFile('/afs/cern.ch/work/c/cantel/private/output/'+run1+'/LLufit_hists_new.root')#TFile('../output/'+run2+'/recon_P4pulses.root')#TFile('../output/'+run2+'/GLufit_hists.root')
	f3 = TFile('/afs/cern.ch/work/c/cantel/private/output/runI/Phos4scan'+run3+'_pedestalcorrected.root')
	f4 = TFile('/afs/cern.ch/work/c/cantel/private/output/runI/Phos4scan'+run4+'_pedestalcorrected.root')
	key="HistfitLLu0x"+TTid
	if option == '1':
		key2="HistfitLLu0x"+TTid2+"_success"



c2 = TCanvas('c2', 'c2', 800, 600)
c2.Divide(2,2)

#d("Hists")

p1exists= False
p2exists= False
p3exists= False
p4exists= False

f1.cd()
#key="reconpulse_part3_id"+TTid
if key in gDirectory.GetListOfKeys():
	p1exists = True
	pulse1=gDirectory.Get(key)
	pulse1.SetMarkerStyle(7)
	c2.cd(1)
	pulse1.Draw("C")

#f2.cd("Hists")
if 'f2' in locals():
	f2.cd()
	if key in gDirectory.GetListOfKeys():
		p2exists = True
		pulse2=gDirectory.Get(key)
		c2.cd(2)
		pulse2.SetMarkerColor(kAzure)
		pulse2.SetMarkerStyle(7)
		pulse2.Draw()

if option == '1':
	f1.cd()
	if key2 in gDirectory.GetListOfKeys():
		p3exists = True
		pulse3=gDirectory.Get(key2)
		c2.cd(3)
		pulse3.SetMarkerColor(kGreen)
		pulse3.SetMarkerStyle(7)
		pulse3.Draw()
	if 'f2' in locals():
		f2.cd()
		if key2 in gDirectory.GetListOfKeys():
			p4exists = True
			pulse4=gDirectory.Get(key2)
			c2.cd(4)
			pulse4.SetMarkerColor(kRed)
			pulse4.SetMarkerStyle(7)
			pulse4.Draw()

else:
	f3.cd()
	keyI="Histcorr0x"+TTid
	if keyI in gDirectory.GetListOfKeys():
		p3exists = True
		pulse3=gDirectory.Get(keyI)
		c2.cd(3)
		pulse3.SetMarkerColor(kGreen)
		pulse3.SetMarkerStyle(7)
		pulse3.Draw()

	f4.cd()
	keyI="Histcorr0x"+TTid
	if keyI in gDirectory.GetListOfKeys():
		p4exists = True
		pulse4=gDirectory.Get(keyI)
		c2.cd(4)
		pulse4.SetMarkerColor(kRed)
		pulse4.SetMarkerStyle(7)
		pulse4.Draw()

c2.WaitPrimitive()


c = TCanvas('c', 'c', 800, 600)
#c.Divide(2,2)

#d("Hists")

if p1exists == True:
	c.cd()
	shiftdown=pulse1.GetBinContent(5)
	for bin in range (0, pulse1.GetNbinsX()+1):
		pulse1.SetBinContent(bin, pulse1.GetBinContent(bin)-shiftdown)
	pulse1.SetStats(0)
	pulse1.SetTitle("comparison pulse TT0x"+TTid+" (part "+region+") diff sigmal: 0.86 , diff sigmar: 4.0 ")
	pulse1.GetXaxis().SetRangeUser(50,350)
	pulse1.Draw("C")

#f2.cd("Hists")

if p2exists == True:
	c.cd()
	shiftdown=pulse2.GetBinContent(5)
	if p1exists== True:
		shift=pulse2.GetMaximumBin()-pulse1.GetMaximumBin()
		temp=pulse2.Clone()
		for bin in range (0, pulse2.GetNbinsX()):
			temp.SetBinContent(bin, pulse2.GetBinContent(bin+shift-2)-shiftdown)
		pulse2=temp
		pulse2.SetStats(0)
		pulse2.Scale(pulse1.GetMaximum()/pulse2.GetMaximum())
		pulse2.Draw("CSAME")
	else:
		pulse2.Draw("C")

if p3exists == True:
	shiftdown=pulse3.GetBinContent(5)
	if p1exists== True:
		shift=pulse3.GetMaximumBin()-pulse1.GetMaximumBin()
		if option == "0":
			shift=shift-6
		temp=pulse3.Clone()
		for bin in range (0, pulse3.GetNbinsX()):
			temp.SetBinContent(bin, pulse3.GetBinContent(bin+shift)-shiftdown)
		temp.Scale(pulse1.GetMaximum()/temp.GetMaximum())
	else:
		shift=pulse3.GetMaximumBin()-pulse2.GetMaximumBin()
		if option == "0":
			shift=shift-6
		temp=pulse3.Clone()
		for bin in range (0, pulse3.GetNbinsX()):
			temp.SetBinContent(bin, pulse3.GetBinContent(bin+shift)-shiftdown)
		temp.Scale(pulse2.GetMaximum()/temp.GetMaximum())
	pulse3=temp
	pulse3.SetStats(0)
	c.cd()
	pulse3.Draw("CSAME")


if p4exists == True:
	shiftdown=pulse4.GetBinContent(5)
	if p1exists== True:
		shift=pulse4.GetMaximumBin()-pulse1.GetMaximumBin()
		if option == "0":
			shift=shift-6
		temp=pulse4.Clone()
		for bin in range (0, pulse4.GetNbinsX()):
			temp.SetBinContent(bin, pulse4.GetBinContent(bin+shift)-shiftdown)
		temp.Scale(pulse1.GetMaximum()/temp.GetMaximum())
	else:
		shift=pulse4.GetMaximumBin()-pulse2.GetMaximumBin()
		if option == "0":
			shift=shift-6
		temp=pulse4.Clone()
		for bin in range (0, pulse4.GetNbinsX()):
			temp.SetBinContent(bin, pulse4.GetBinContent(bin+shift)-shiftdown)
		temp.Scale(pulse2.GetMaximum()/temp.GetMaximum())
	pulse4=temp
	pulse4.SetStats(0)

	c.cd()
	pulse4.Draw("CSAME")

c.WaitPrimitive()


		

