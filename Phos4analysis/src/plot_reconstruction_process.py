from ROOT import *

f=TFile("264603/recon_P4pulses.root")

f.cd("Hists")

recon=gDirectory.Get('reconpulse_part1_id02190901')

c=TCanvas("c", "c", 800, 600)
c.cd()
recon.Draw()

#col_vec=[kOrange+0, kRed+1, kRed+3, kMagenta+2, kBlue+3, kBlue-3, kAzure+3, kAzure+1, kGreen+1, kGreen+3] #list(range(40,50))#+list(range(30,39))

col_vec=[kOrange+0, kAzure+1, kGreen+3] #list(range(40,50))#+list(range(30,39))
histlist=[]
hcount=0

for i in range(0, 30, 10):
	histlist.append(TH1D("hist_step"+str(i), "adc curve", 360, 0, 374.4))
	for bin in range(0,recon.GetNbinsX()):
		if (bin-i)%(25)==0:
			print "i = "+str(i)+" bin = "+str(bin)
			histlist[hcount].SetBinContent(bin, recon.GetBinContent(bin))
	hcount+=1

recon.SetMarkerColor(0)
col=0;
print len(histlist)
print len(col_vec)
for h in histlist:
	h.Rebin(5)
	h.SetLineColor(col_vec[col])
	h.SetFillColor(col_vec[col])
	h.Draw("SAME")
	print col
	col+=1
c.WaitPrimitive()
