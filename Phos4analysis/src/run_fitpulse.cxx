#include "TTxml.h"
#include <map>
#include "TH2TT.h"
#include "FitFunctions.h"
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <stdio.h>

#include <TH1.h>
#include <TH2.h>
#include "TStyle.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TFile.h"
#include "TMath.h"

#include "TF1.h"

// runs: 
// LAr: "273449" "273450" (25/07/2015) "269939", "269942" (02/07/2015) "264603", "264604" (12/05/2015), "294097", "294104" (01/04/2016)
// Tile: "272641" (20/07) "264505" (11/05) "268548" (random at 80MHZ)  "289656" (11/02/2016) "294117" (01/04/2016)

void fitPulserRun(const char *run, const char *inputfile="recon_P4pulses.root", const char *outputfile_ref="reffit_hists_new.root", const char *outputfile_GLu="GLufit_hists_new.root", const char * outputfile_LLu="LLufit_hists_new.root", const char * outputfile_fitquality="fitquality_hists_new.root"){

	std::cout<<"executing fitting to phos4"<<std::endl;

        char inputpath_and_file[64];
        sprintf(inputpath_and_file, "/afs/cern.ch/work/c/cantel/private/output/%s/%s", run, inputfile);
	TFile in_file(inputpath_and_file);
        char outputdir_ref[64];
        sprintf(outputdir_ref, "/afs/cern.ch/work/c/cantel/private/output/%s/%s", run, outputfile_ref);
	TFile out_file_ref(outputdir_ref,"UPDATE");
        char outputdir_GLu[64];
        sprintf(outputdir_GLu, "/afs/cern.ch/work/c/cantel/private/output/%s/%s", run, outputfile_GLu);
	TFile out_file_GLu(outputdir_GLu,"UPDATE");
        char outputdir_LLu[64];
        sprintf(outputdir_LLu, "/afs/cern.ch/work/c/cantel/private/output/%s/%s", run, outputfile_LLu);
	TFile out_file_LLu(outputdir_LLu,"UPDATE");
        char outputdir_fitquality[64];
        sprintf(outputdir_fitquality, "/afs/cern.ch/work/c/cantel/private/output/%s/%s", run, outputfile_fitquality);
	TFile out_file_fitquality(outputdir_fitquality,"UPDATE");

	ofstream m_output;
	m_output.open("outputfile.txt");


	std::cout<<"all files created."<<std::endl;

	TH1 *hist;
	int threshold = 60;
	int threshold_red = 50;
	int maxchi2ndf = 20;
  	double step_to_ns = 25./24.; 

	//Peak maximum position and value
	int maxbin;
	double maxvalue;
	double maxpos;

	//Fitting range for reference fit
	double minRange = 0;
	double maxRange = 0;
	//Fitting ranges for the other fits
	double minRangeGLu = 0;
	double maxRangeGLu = 0;
	double minRangeGLuFCal = 0;
	double maxRangeGLuFCal = 0;
	double minRangeLLu = 0;
	double maxRangeLLu = 0;
	double minRangeLLuFCal = 0;
	double maxRangeLLuFCal = 0;

	//possible CoolIDs
	unsigned int CoolID;

	//counter variable for saturated pulses
	int nsat = 0;
	int nnopulse = 0;

	//counter variables for fits
	int nfit = 0;
	int nfitref = 0;
	int nfailfitref = 0;
	int nfailfitGLu = 0;
	int nfailfitLLu = 0;

/*
	//pedestal values per channel
	std::map<unsigned int,double> Pedestals;
	TTxml xml_in;
	char xml_in_filename[100];
	sprintf(xml_in_filename,"output/Run%i/PedestalsRun%i.xml",runNo,runNo);
	xml_in.Read(xml_in_filename);
	Pedestals = xml_in.Get("pedestal");
	//Disabled channels
	std::map<unsigned int,double> DisabledChannels;
	DisabledChannels = xml_in.Get("disabled");
	//BadCalo channels
	std::map<unsigned int,double> BadCaloChannels;
	BadCaloChannels = xml_in.Get("badCalo");
	//Saturated channels
	std::map<unsigned int,double> SaturatedChannels;
	SaturatedChannels = xml_in.Get("saturated");
*/

//------------------------------------------------------------------------------------------------         
   //definitions of the histograms for the deviation plots
        //Histogram for the reference fit

        TH2TT *emlay_overview = new TH2TT("emlay_overview","overview (saturated, no pulse, fitable)");
        emlay_overview->GetXaxis()->SetTitle("eta");
        emlay_overview->GetYaxis()->SetTitle("phi");
        emlay_overview->Fillall(-1000);
        TH2TT *hadlay_overview = new TH2TT("hadlay_overview","overview (saturated, no pulse, fitable)");
        hadlay_overview->GetXaxis()->SetTitle("eta");
        hadlay_overview->GetYaxis()->SetTitle("phi");
        hadlay_overview->Fillall(-1000);

        TH2 *histallfitsRef = new TH2D("allfitsRef","#All Fits depending on calodivision and pulse height;CaloDivision;Pulse Height [ADC counts]",9,-0.5,8.5,64,-0.5,1023.5);

        TH2 *histfitfailRef = new TH2D("failedfitsRef","#Failed Fits depending on calodivision and pulse height;CaloDivision;Pulse Height [ADC counts]",9,-0.5,8.5,64,-0.5,1023.5);

        TH2 *histQsuccessFitRef = new TH2D("QsuccessFitRef","Quota of fit success depending on calodivision and pulse height;CaloDivision;Pulse Height [ADC counts]",9,-0.5,8.5,64,-0.5,1023.5);

        TH1 *histmaxposRef = new TH1D("histmaxposRef","histmaxposRef",200,-10.,10.);
        histmaxposRef->GetXaxis()->SetTitle("maxpos_fit-maxbincenter_hist");
        histmaxposRef->GetYaxis()->SetTitle("number of trigger towers");
       
        TH1 *histamplRef = new TH1D("histamplRef","histamplRef",200,-0.1,0.1);
        histamplRef->GetXaxis()->SetTitle("(ampl_fit-corr.maxheight_hist)/ampl_fit");
        histamplRef->GetYaxis()->SetTitle("number of trigger towers");

        TH1 *histchi2Ref = new TH1D("histchi2Ref","Chi2/ndf - Reference fit",maxchi2ndf,0,maxchi2ndf);
        histchi2Ref->GetXaxis()->SetTitle("chi2/ndf");
        histchi2Ref->GetYaxis()->SetTitle("number of trigger towers");

        TH2TT *emlaychi2Ref = new TH2TT("emlaychi2Ref","chi2_ndf (em layer) for reference fit");
        emlaychi2Ref->GetXaxis()->SetTitle("eta");
        emlaychi2Ref->GetYaxis()->SetTitle("phi");
       
        TH2TT *hadlaychi2Ref = new TH2TT("hadlaychi2Ref","chi2_ndf (had layer) for reference fit");
        hadlaychi2Ref->GetXaxis()->SetTitle("eta");
        hadlaychi2Ref->GetYaxis()->SetTitle("phi");
       
        TH2TT *emlaymaxposRef = new TH2TT("emlaymaxposRef","maximum position deviation (em layer) for reference fit");
        emlaymaxposRef->GetXaxis()->SetTitle("eta");
        emlaymaxposRef->GetYaxis()->SetTitle("phi");
        emlaymaxposRef->Fillall(-1000);
       
        TH2TT *hadlaymaxposRef = new TH2TT("hadlaymaxposRef","maximum position deviation (had layer) for reference fit");
        hadlaymaxposRef->GetXaxis()->SetTitle("eta");
        hadlaymaxposRef->GetYaxis()->SetTitle("phi");
        hadlaymaxposRef->Fillall(-1000);
       
        TH2TT *emlayamplRef = new TH2TT("emlayamplRef","amplitude deviation (em layer) for reference fit");
        emlayamplRef->GetXaxis()->SetTitle("eta");
        emlayamplRef->GetYaxis()->SetTitle("phi");
        emlayamplRef->Fillall(-1000);
       
        TH2TT *hadlayamplRef = new TH2TT("hadlayamplRef","amplitude deviation (had layer) for reference fit");
        hadlayamplRef->GetXaxis()->SetTitle("eta");
        hadlayamplRef->GetYaxis()->SetTitle("phi");
        hadlayamplRef->Fillall(-1000);

        //Histograms for Gauss-Landau fit with undershoot - appr. 4 bunch range fit
        TH2 *histallfitsGLu = new TH2D("allfitsGLu","#All Fits depending on calodivision and pulse height;CaloDivision;Pulse Height [ADC counts]",9,-0.5,8.5,64,-0.5,1023.5);

        TH2 *histfitfailGLu = new TH2D("failedfitsGLu","#Failed Fits depending on calodivision and pulse height;CaloDivision;Pulse Height [ADC counts]",9,-0.5,8.5,64,-0.5,1023.5);

        TH2 *histQsuccessFitGLu = new TH2D("QsuccessFitGLu","Quota of fit success depending on calodivision and pulse height;CaloDivision;Pulse Height [ADC counts]",9,-0.5,8.5,64,-0.5,1023.5);

	TH1 *histmaxposGLu = new TH1D("histmaxposGLu","Maximum position deviation - GLu",200,-10.,10.);
	histmaxposGLu->GetXaxis()->SetTitle("delta_maxpos [ns]");
	histmaxposGLu->GetYaxis()->SetTitle("number of trigger towers");

	TH1 *histampGLu = new TH1D("histampGLu","Amplitude deviation - GLu",200,-0.1,0.1);
	histampGLu->GetXaxis()->SetTitle("relative delta_amp");
	histampGLu->GetYaxis()->SetTitle("number of trigger towers");

	TH1 *histchi2GLu = new TH1D("histchi2GLu","Chi2/ndf - GLu",maxchi2ndf,0,maxchi2ndf);
	histchi2GLu->GetXaxis()->SetTitle("chi2/ndf");
	histchi2GLu->GetYaxis()->SetTitle("number of trigger towers");
       
        TH2TT *emlaychi2GLu = new TH2TT("emlaychi2GLu","chi2_ndf (em layer) for GLu");
        emlaychi2GLu->GetXaxis()->SetTitle("eta");
        emlaychi2GLu->GetYaxis()->SetTitle("phi");
       
        TH2TT *hadlaychi2GLu = new TH2TT("hadlaychi2GLu","chi2_ndf (had layer) for GLu");
        hadlaychi2GLu->GetXaxis()->SetTitle("eta");
        hadlaychi2GLu->GetYaxis()->SetTitle("phi");
       
        TH2TT *emlaymaxposGLu = new TH2TT("emlaymaxposGLu","maximum position deviation (em layer) for GLu");
        emlaymaxposGLu->GetXaxis()->SetTitle("eta");
        emlaymaxposGLu->GetYaxis()->SetTitle("phi");
        emlaymaxposGLu->Fillall(-1000);
       
        TH2TT *hadlaymaxposGLu = new TH2TT("hadlaymaxposGLu","maximum position deviation (had layer) for GLu");
        hadlaymaxposGLu->GetXaxis()->SetTitle("eta");
        hadlaymaxposGLu->GetYaxis()->SetTitle("phi");
        hadlaymaxposGLu->Fillall(-1000);
       
        TH2TT *emlayamplGLu = new TH2TT("emlayamplGLu","amplitude deviation (em layer) for GLu");
        emlayamplGLu->GetXaxis()->SetTitle("eta");
        emlayamplGLu->GetYaxis()->SetTitle("phi");
        emlayamplGLu->Fillall(-1000);
       
        TH2TT *hadlayamplGLu = new TH2TT("hadlayamplGLu","amplitude deviation (had layer) for GLu");
        hadlayamplGLu->GetXaxis()->SetTitle("eta");
        hadlayamplGLu->GetYaxis()->SetTitle("phi");
        hadlayamplGLu->Fillall(-1000);
       
        TH2TT *emlaysigm1GLu = new TH2TT("emlaysigm1GLu","sigma_gauss (em layer) for GLu");
        emlaysigm1GLu->GetXaxis()->SetTitle("eta");
        emlaysigm1GLu->GetYaxis()->SetTitle("phi");
       
        TH2TT *hadlaysigm1GLu = new TH2TT("hadlaysigm1GLu","sigma_gauss (had layer) for GLu");
        hadlaysigm1GLu->GetXaxis()->SetTitle("eta");
        hadlaysigm1GLu->GetYaxis()->SetTitle("phi");
       
        TH2TT *emlaysigm2GLu = new TH2TT("emlaysigm2GLu","sigma_landau (em layer) for GLu");
        emlaysigm2GLu->GetXaxis()->SetTitle("eta");
        emlaysigm2GLu->GetYaxis()->SetTitle("phi");
       
        TH2TT *hadlaysigm2GLu = new TH2TT("hadlaysigm2GLu","sigma_landau (had layer) for GLu");
        hadlaysigm2GLu->GetXaxis()->SetTitle("eta");
        hadlaysigm2GLu->GetYaxis()->SetTitle("phi");
       
        //Histograms for Landau-Landau fit with undershoot - appr. 4 bunch range fit
        TH2 *histallfitsLLu = new TH2D("allfitsLLu","#All Fits depending on calodivision and pulse height;CaloDivision;Pulse Height [ADC counts]",9,-0.5,8.5,64,-0.5,1023.5);

        TH2 *histfitfailLLu = new TH2D("failedfitsLLu","#Failed Fits depending on calodivision and pulse height;CaloDivision;Pulse Height [ADC counts]",9,-0.5,8.5,64,-0.5,1023.5);

        TH2 *histQsuccessFitLLu = new TH2D("QsuccessFitLLu","Quota of fit success depending on calodivision and pulse height;CaloDivision;Pulse Height [ADC counts]",9,-0.5,8.5,64,-0.5,1023.5);

	TH1 *histmaxposLLu = new TH1D("histmaxposLLu","Maximum position deviation - LLu",200,-10.,10.);
	histmaxposLLu->GetXaxis()->SetTitle("delta_maxpos [ns]");
	histmaxposLLu->GetYaxis()->SetTitle("number of trigger towers");

	TH1 *histampLLu = new TH1D("histampLLu","Amplitude deviation - LLu",200,-0.1,0.1);
	histampLLu->GetXaxis()->SetTitle("relative delta_amp");
	histampLLu->GetYaxis()->SetTitle("number of trigger towers");

	TH1 *histchi2LLu = new TH1D("histchi2LLu","Chi2/ndf - LLu",maxchi2ndf,0,maxchi2ndf);
	histchi2LLu->GetXaxis()->SetTitle("chi2/ndf");
	histchi2LLu->GetYaxis()->SetTitle("number of trigger towers");
       
        TH2TT *emlaychi2LLu = new TH2TT("emlaychi2LLu","chi2_ndf (em layer) for LLu");
        emlaychi2LLu->GetXaxis()->SetTitle("eta");
        emlaychi2LLu->GetYaxis()->SetTitle("phi");
       
        TH2TT *hadlaychi2LLu = new TH2TT("hadlaychi2LLu","chi2_ndf (had layer) for LLu");
        hadlaychi2LLu->GetXaxis()->SetTitle("eta");
        hadlaychi2LLu->GetYaxis()->SetTitle("phi");
       
        TH2TT *emlaymaxposLLu = new TH2TT("emlaymaxposLLu","maximum position deviation (em layer) for LLu");
        emlaymaxposLLu->GetXaxis()->SetTitle("eta");
        emlaymaxposLLu->GetYaxis()->SetTitle("phi");
        emlaymaxposLLu->Fillall(-1000);
       
        TH2TT *hadlaymaxposLLu = new TH2TT("hadlaymaxposLLu","maximum position deviation (had layer) for LLu");
        hadlaymaxposLLu->GetXaxis()->SetTitle("eta");
        hadlaymaxposLLu->GetYaxis()->SetTitle("phi");
        hadlaymaxposLLu->Fillall(-1000);
       
        TH2TT *emlayamplLLu = new TH2TT("emlayamplLLu","amplitude deviation (em layer) for LLu");
        emlayamplLLu->GetXaxis()->SetTitle("eta");
        emlayamplLLu->GetYaxis()->SetTitle("phi");
        emlayamplLLu->Fillall(-1000);
       
        TH2TT *hadlayamplLLu = new TH2TT("hadlayamplLLu","amplitude deviation (had layer) for LLu");
        hadlayamplLLu->GetXaxis()->SetTitle("eta");
        hadlayamplLLu->GetYaxis()->SetTitle("phi");
        hadlayamplLLu->Fillall(-1000);
       
        TH2TT *emlaysigm1LLu = new TH2TT("emlaysigm1LLu","sigma_left (em layer) for LLu");
        emlaysigm1LLu->GetXaxis()->SetTitle("eta");
        emlaysigm1LLu->GetYaxis()->SetTitle("phi");
       
        TH2TT *hadlaysigm1LLu = new TH2TT("hadlaysigm1LLu","sigma_left (had layer) for LLu");
        hadlaysigm1LLu->GetXaxis()->SetTitle("eta");
        hadlaysigm1LLu->GetYaxis()->SetTitle("phi");
       
        TH2TT *emlaysigm2LLu = new TH2TT("emlaysigm2LLu","sigma_right (em layer) for LLu");
        emlaysigm2LLu->GetXaxis()->SetTitle("eta");
        emlaysigm2LLu->GetYaxis()->SetTitle("phi");
       
        TH2TT *hadlaysigm2LLu = new TH2TT("hadlaysigm2LLu","sigma_right (had layer) for LLu");
        hadlaysigm2LLu->GetXaxis()->SetTitle("eta");
        hadlaysigm2LLu->GetYaxis()->SetTitle("phi");
       
        //Histograms for undershoot mean estimate
        //GLu
        TH1* histundEMBGLu = new TH1D("undershootEMB_GLu","undershoot vs amplitude EMB fitGLu",200,0,1);
        histundEMBGLu->GetXaxis()->SetTitle("undershoot/amplitude");
        histundEMBGLu->GetYaxis()->SetTitle("number of channels");
       
        TH1* histundEMECGLu1 = new TH1D("undershootEMEC_GLu_1","undershoot vs amplitude EMEC fitGLu - |eta|<2.5",200,0,1);
        histundEMECGLu1->GetXaxis()->SetTitle("undershoot/amplitude");
        histundEMECGLu1->GetYaxis()->SetTitle("number of channels");
       
        TH1* histundEMECGLu2 = new TH1D("undershootEMEC_GLu_2","undershoot vs amplitude EMEC fitGLu - |eta|>2.5",200,0,1);
        histundEMECGLu2->GetXaxis()->SetTitle("undershoot/amplitude");
        histundEMECGLu2->GetYaxis()->SetTitle("number of channels");
       
        TH1* histundFCalGLu = new TH1D("undershootFCal_GLu","undershoot vs amplitude FCal fitGLu",200,0,1);
        histundFCalGLu->GetXaxis()->SetTitle("undershoot/amplitude");
        histundFCalGLu->GetYaxis()->SetTitle("number of channels");
       
        TH1* histundHECGLu = new TH1D("undershootHEC_GLu","undershoot vs amplitude HEC fitGLu",200,0,1);
        histundHECGLu->GetXaxis()->SetTitle("undershoot/amplitude");
        histundHECGLu->GetYaxis()->SetTitle("number of channels");
       
        TH1* histundTileGLu = new TH1D("undershootTile_GLu","undershoot vs amplitude Tile fitGLu",200,0,1);
        histundTileGLu->GetXaxis()->SetTitle("undershoot/amplitude");
        histundTileGLu->GetYaxis()->SetTitle("number of channels");
       
        //LLu
        TH1* histundEMBLLu = new TH1D("undershootEMB_LLu","undershoot vs amplitude EMB fitLLu",200,0,1);
        histundEMBLLu->GetXaxis()->SetTitle("undershoot/amplitude");
        histundEMBLLu->GetYaxis()->SetTitle("number of channels");
       
        TH1* histundEMECLLu1 = new TH1D("undershootEMEC_LLu_1","undershoot vs amplitude EMEC fitLLu - |eta|<2.5",200,0,1);
        histundEMECLLu1->GetXaxis()->SetTitle("undershoot/amplitude");
        histundEMECLLu1->GetYaxis()->SetTitle("number of channels");
       
        TH1* histundEMECLLu2 = new TH1D("undershootEMEC_LLu_2","undershoot vs amplitude EMEC fitLLu - |eta|>2.5",200,0,1);
        histundEMECLLu2->GetXaxis()->SetTitle("undershoot/amplitude");
        histundEMECLLu2->GetYaxis()->SetTitle("number of channels");
       
        TH1* histundFCalLLu = new TH1D("undershootFCal_LLu","undershoot vs amplitude FCal fitLLu",200,0,1);
        histundFCalLLu->GetXaxis()->SetTitle("undershoot/amplitude");
        histundFCalLLu->GetYaxis()->SetTitle("number of channels");
       
        TH1* histundHECLLu = new TH1D("undershootHEC_LLu","undershoot vs amplitude HEC fitLLu",200,0,1);
        histundHECLLu->GetXaxis()->SetTitle("undershoot/amplitude");
        histundHECLLu->GetYaxis()->SetTitle("number of channels");
       
        TH1* histundTileLLu = new TH1D("undershootTile_LLu","undershoot vs amplitude Tile fitLLu",200,0,1);
        histundTileLLu->GetXaxis()->SetTitle("undershoot/amplitude");
        histundTileLLu->GetYaxis()->SetTitle("number of channels");
       
//-----------------------------------------------------------------------------------------------------
       
	//definition of all bad channels
	std::vector<char*> badchannel;
	bool bad = false;

	//include maps for CaloDivision
	std::map <unsigned int, double> part_map;
	std::map <unsigned int, double> eta_map;
	std::map <unsigned int, double> phi_map;
	std::map <unsigned int, double> layer_map;
	std::map <unsigned int, double> ped_map;
	std::map <unsigned int, double> sat_map;
	std::map <unsigned int, double> nopulse_map;

	//include maps for parameters
	std::map <unsigned int, double> sigmal_map;
	std::map <unsigned int, double> sigmar_map;
	std::map <unsigned int, double> us_map;
	std::map <unsigned int, double> DA_map;
	std::map <unsigned int, double> chi2ndf_map;
	std::map <unsigned int, double> func_map;

	TTxml m_par;
	//sigmal_map=m_par.Get("sigmal)
	ifstream ifile("/afs/cern.ch/work/c/cantel/private/output/TTparameters.xml");
	if (ifile) {
		m_par.Read("/afs/cern.ch/work/c/cantel/private/output/TTparameters.xml");
		sigmal_map = m_par.Get("sigmal");
		sigmar_map = m_par.Get("sigmar");
		us_map = m_par.Get("us");
		DA_map = m_par.Get("undVsAmpl");
		chi2ndf_map = m_par.Get("chi2ndf");
		func_map = m_par.Get("func");
	}

	TTxml m;
        char xmlname[100];
	sprintf(xmlname, "/afs/cern.ch/work/c/cantel/private/output/%s/TTproperties_run%s.xml", run, run); 
	m.Read(xmlname);
	part_map = m.Get("parts");
	eta_map = m.Get("etas");
	phi_map = m.Get("phis");
	layer_map = m.Get("layers");
	ped_map = m.Get("pedestal");
	sat_map = m.Get("saturated");
	nopulse_map = m.Get("nosignal");
//-----------------------------------------------------------------------------------------
   	//int cnt=0;
   //Schleifendurchlauf durch alle Histogramme
        for (std::map<unsigned int, double>::iterator i =eta_map.begin(); i!=eta_map.end(); i++){ // #1 : loop through ids
		//if (cnt>10) continue;
		//cnt++;

		CoolID = i->first;
		int part = (int) part_map[CoolID];
		double eta = (double) eta_map[CoolID];
		double phi = (double) phi_map[CoolID];
		int layer = (int) layer_map[CoolID];
/*
		//jump channel if it is disabled
		if ( (int) DisabledChannels[CoolID] == 1 ) continue;  
		//jump channel if it is flagged as badCalo
		if ( (int) BadCaloChannels[CoolID] == 1 ) continue; */ 
		//jump channel if it is saturated 
		if ( (int) sat_map[CoolID] == 1 ){
			if (layer==0) emlay_overview->SetBinContent(eta, phi, -2);
			else hadlay_overview->SetBinContent(eta, phi, -2);
			nsat++;
			continue;
		} 
		//jump channel if there was no pulse 
		if ( (int) nopulse_map[CoolID] == 1 ){
			if (layer==0) emlay_overview->SetBinContent(eta, phi, -1);
			else hadlay_overview->SetBinContent(eta, phi, -1);
			nnopulse++;
			continue;
		}          

		in_file.cd("Hists");

		char histName[30];
		sprintf(histName,"reconpulse_part%d_id%08x",part, CoolID);
        	//std::cout<<"hist name :"<<histName<<std::endl;
		//gDirectory->Print();
		gDirectory->GetObject(histName,hist);

		if (hist != 0){ // #2 : if hist exists...
                	maxbin = hist->GetMaximumBin();
                	maxvalue = hist->GetBinContent(maxbin);
			maxpos = hist->GetXaxis()->GetBinCenter(maxbin);

                        //std::cout<<"hist exists"<<std::endl;
/*                       
		        //exclusion of bad channels from the fitting procedure
		        for (unsigned int k = 0; k!=badchannel.size();k++){
		                if (strcmp(histName,badchannel[k])==0) {
	//                              std::cout<<histName<<" = bad channel"<<std::endl;
		                        bad = true;
		                        break;
		                }
			}
*/                      if (maxvalue<threshold) continue;
			if (maxvalue>=1020) continue;

		        double pedestal = 32; //Pedestals[CoolID];
               
               	 	//Exclusion of all signals that are a bad channel or don't show any signal -> threshold 60 ADC counts
                	//(reduced threshold for TTs with |eta|=3.15 -> 50 ADC counts)
			if ( maxvalue < 1022 || maxvalue >= threshold || ( maxvalue >= threshold_red && fabs(eta)>3.1 && fabs(eta)<3.2 ) ){ // here excluded !(bad) && // #3 : if above threshold (or bad).. ***
				//if (sigmal_map.count(CoolID)) continue;

				if (layer==0) emlay_overview->SetBinContent(eta, phi, 1);
				else hadlay_overview->SetBinContent(eta, phi, 1);
		                //--------------------------------------------
		                //Determination of fitting ranges
		                //For reference fit
		                minRange = step_to_ns*((maxbin-1)-13);
		                maxRange = step_to_ns*((maxbin-1)+13)
;
		                //For Landau-Landau fit  with undershoot - small range
		                minRangeGLu = step_to_ns*(maxbin-1)-25;
		                maxRangeGLu = step_to_ns*(maxbin-1)+50;
		                minRangeGLuFCal = step_to_ns*(maxbin-1)-25;
		                maxRangeGLuFCal = step_to_ns*(maxbin-1)+25;
		                //For Landau-Landau fit with undershoot - small range
		                minRangeLLu = minRangeGLu;
		                maxRangeLLu = maxRangeGLu;
		                minRangeLLuFCal = minRangeGLuFCal;
		                maxRangeLLuFCal = maxRangeGLuFCal;
		               
                        //---------------------------------------------------------
				std::cout<<"max bin error... "<<hist->GetBinError(maxbin)<<std::endl;                       		

		                //Fit of histogram peak to determine the reference values
		                TF1 *reffunc;
		                if (part == 6) {//HEC --> use LL as reference fit
		                        reffunc = new TF1("reffunc",Fit_LL,minRange,maxRange,5);
		                }
		                else reffunc = new TF1("reffunc",Fit_GL,minRange,maxRange,5); //for all other CaloDivisions use GL as reference fit
			        reffunc->SetParNames("Maximumposition","Amplitude","sigmaLeft","sigmaRight","pedestal");
			        reffunc->SetParameter(0,minRange+20.);
			        reffunc->SetParameter(1,0.95*maxvalue*TMath::Exp(0.5));
			        reffunc->SetParameter(2,10.);
			        reffunc->SetParameter(3,10.);
			        reffunc->FixParameter(4,pedestal);
			        reffunc->SetParLimits(0,minRange,maxRange);
			        reffunc->SetParLimits(1,21,1700); //recall reduction factor: limits are((45-pedestal)*exp(0.5),1024*exp(0.5))
			        reffunc->SetParLimits(2,5,50);
			        reffunc->SetParLimits(3,5,50);
			        reffunc->SetLineColor(4);

				int fitreturn=-100;
			       
			        fitreturn= (int) hist->Fit("reffunc","RWQM");
	                        std::cout<<"fit return is "<<fitreturn<<std::endl;

			        nfitref++;
			        histallfitsRef->Fill(part, maxvalue);
			       
			        double *refpar = new double[4];
			        refpar = reffunc->GetParameters();
			        double refchi2 = reffunc->GetChisquare();
			        int refndf = reffunc->GetNDF();
				m_output << "max adc is ... "<<maxvalue<<"\n";

                       	 	std::cout << "chi2 for ref fit... " << refchi2 << std::endl;
                       	 	std::cout << "ndf for ref fit... " << refndf << std::endl;
			        if ( refchi2/refndf <= maxchi2ndf ) histchi2Ref->Fill(refchi2/refndf);
			       
			        char newHistName[30];
			        sprintf(newHistName,"Histfitrefpart%dID%uEta%2.1fPhi%2.1fChi%2.1f", part, CoolID,eta,phi,refchi2/refndf);
			        hist->SetName(newHistName);
				char newHistTitle[30];
				sprintf(newHistTitle, "fitted hist (chi2/ndf = %f)", refchi2/refndf);
				hist->SetTitle(newHistTitle);
			        out_file_ref.cd();
			        hist->Write();
	                       
	                	if (refchi2/refndf<=20){ // #4 : passed ref fit

	                                std::cout<<"successful reference fit for channel:\t"<<histName<<"\tchi2/ndf\t"<<refchi2/refndf<<"\t--> maxbin/maxvalue\t"<<maxbin<<"/"<<maxvalue<<std::endl;

			                double deltamax = refpar[0]-(hist->GetBinCenter(maxbin));
			                histmaxposRef->Fill(deltamax);
			                double deltaamp = (refpar[1]-(maxvalue-pedestal)*TMath::Exp(0.5))/refpar[1];
			                histamplRef->Fill(deltaamp);

			                if (layer==0){
			                                emlaychi2Ref->Fill(eta,phi,refchi2/refndf);
			                                emlaymaxposRef->Fill(eta,phi,deltamax+1000);
			                                emlayamplRef->Fill(eta,phi,deltaamp+1000);
			                }
			                else if (layer==1){
			                                hadlaychi2Ref->Fill(eta,phi,refchi2/refndf);
			                                hadlaymaxposRef->Fill(eta,phi,deltamax+1000);
			                                hadlayamplRef->Fill(eta,phi,deltaamp+1000);
			                }

			                //----------------------------------------------------------------------------
			                //Fitting the histogram with the different functions
			                //Fit of the histogram with a Gauss Landau function with undershoot - small range
					if (part == 3 || part == 7 || part == 8){
					        TF1 *fitGLu;
					        if (part == 0 || part == 4 || part == 5) {//this is FCal -->uses other fit range
					                fitGLu = new TF1("fitGLu",Fit_GLu,minRangeGLuFCal,maxRangeGLuFCal,6);
					        }
					        else fitGLu = new TF1("fitGLu",Fit_GLu,minRangeGLu,maxRangeGLu,6);
					        fitGLu->SetParNames("Maximumposition","Amplitude","sigmaGauss","sigmaLandau","undershoot","pedestal");
					        fitGLu->SetParameter(0,minRange+20.);
					        fitGLu->SetParameter(1,80.);
					        fitGLu->SetParameter(2,10.);
					        fitGLu->SetParameter(3,10.);
					        fitGLu->SetParameter(4,1.);
					        fitGLu->FixParameter(5,pedestal);
					        fitGLu->SetParLimits(0,minRangeGLu,maxRangeGLu);
					        fitGLu->SetParLimits(1,21,1700);
					        fitGLu->SetParLimits(2,5,50);
					        fitGLu->SetParLimits(3,5,50);
					        fitGLu->SetParLimits(4,0,350);
					        fitGLu->SetLineColor(4);
					       
						int GLufitreturn=1;
						
					        GLufitreturn= (int)hist->Fit("fitGLu","RQW");
					        nfit++;
					        histallfitsGLu->Fill(part,maxvalue);

	                        		std::cout<<"GLu fit return is "<<GLufitreturn<<std::endl;
					       
					        double chi2GLu = fitGLu->GetChisquare();
					        int ndfGLu = fitGLu->GetNDF();
					        double *parfitGLu = new double[5];
					        double *pardiffGLu = new double[2];
					        parfitGLu = fitGLu->GetParameters();
					        pardiffGLu[0] = parfitGLu[0]-refpar[0];
					        pardiffGLu[1] = (parfitGLu[1]-refpar[1])/refpar[1];

					        char newHistNameGLu[50];
										       
				        	if (chi2GLu/ndfGLu <= maxchi2ndf ){//&& GLufitreturn==0){ 

					        	sprintf(newHistNameGLu,"HistfitGLu0x%08x_success",CoolID);
					        	hist->SetName(newHistNameGLu);
					        	out_file_GLu.cd();
					        	hist->Write();

							char newerHistTitle[50];
							sprintf(newerHistTitle, "fitted hist (sigl = %f sigr = %f chi2/ndf = %f)", parfitGLu[2], parfitGLu[3], chi2GLu/ndfGLu);
							hist->SetTitle(newerHistTitle);

							double DA = parfitGLu[4]/parfitGLu[1];
							if (sigmal_map.count(CoolID)){
								if (chi2ndf_map[CoolID]>(chi2GLu/ndfGLu)){
									sigmal_map[CoolID]=parfitGLu[2];
									sigmar_map[CoolID]=parfitGLu[3];
									us_map[CoolID]=parfitGLu[4];
									DA_map[CoolID]=DA;
									chi2ndf_map[CoolID]=(chi2GLu/ndfGLu);
									func_map[CoolID]=1.;
								}
							}
							else {
								sigmal_map[CoolID]=parfitGLu[2];
								sigmar_map[CoolID]=parfitGLu[3];
								us_map[CoolID]=parfitGLu[4];
								DA_map[CoolID]=DA;
								chi2ndf_map[CoolID]=(chi2GLu/ndfGLu);
								func_map[CoolID]=1.;
							}
					

					                histmaxposGLu->Fill(pardiffGLu[0]);
					                histampGLu->Fill(pardiffGLu[1]);
					                histchi2GLu->Fill(chi2GLu/ndfGLu);
					                if (layer==0){
					                        emlaychi2GLu->Fill(eta,phi,chi2GLu/ndfGLu);
					                        emlaymaxposGLu->Fill(eta,phi,pardiffGLu[0]+1000);
					                        emlayamplGLu->Fill(eta,phi,pardiffGLu[1]+1000);
					                        emlaysigm1GLu->Fill(eta,phi,parfitGLu[2]);
					                        emlaysigm2GLu->Fill(eta,phi,parfitGLu[3]);
					                }
					                else if (layer==1){
					                        hadlaychi2GLu->Fill(eta,phi,chi2GLu/ndfGLu);
					                        hadlaymaxposGLu->Fill(eta,phi,pardiffGLu[0]+1000);
					                        hadlayamplGLu->Fill(eta,phi,pardiffGLu[1]+1000);
					                        hadlaysigm1GLu->Fill(eta,phi,parfitGLu[2]);
					                        hadlaysigm2GLu->Fill(eta,phi,parfitGLu[3]);
					                }
					               
					                if ( part == 3 ){ //EMB
					                        histundEMBGLu->Fill(DA);
					                }
					                if ( part == 1 ){ //EMEC
					                        if ( -2.5 <= eta && eta <= 2.5 ) histundEMECGLu1->Fill(DA);
					                        else histundEMECGLu2->Fill(DA);
					                }
					                if ( part == 6 ){ //HEC
					                        histundHECGLu->Fill(DA);
					                }
					                if ( part == 0 || part == 4 || part == 5){ //FCal
					                        histundFCalGLu->Fill(DA);
					                }
					                if (part == 7 || part == 8){ //Tile
					                        histundTileGLu->Fill(DA);
					                }
					        } // close of passed GLU fit
					        else {
					        	sprintf(newHistNameGLu,"HistfitGLu0x%08x_failed",CoolID);
					        	hist->SetName(newHistNameGLu);
					        	out_file_GLu.cd();
					        	hist->Write();
					                nfailfitGLu++;
					                histfitfailGLu->Fill(part,maxvalue);
					        }
					}
//                              else std::cout<<histName<<"\tGauss-Landau fit w.u. sm\tchi2/ndf=\t"<<chi2GLu/ndfGLu<<std::endl;

	                        //Fit of the histogram with a Landau Landau function with undershoot - small fitting range
					else {
					        TF1 *fitLLu;
					        if (part == 0 || part == 4 || part == 5) {//FCal -->use other fit range
					                fitLLu = new TF1("fitLLu",Fit_LLu,minRangeLLuFCal,maxRangeLLuFCal,6);
					        }
					        else
					        fitLLu = new TF1("fitLLu",Fit_LLu,minRangeLLu,maxRangeLLu,6);
					        fitLLu->SetParNames("Maximumposition","Amplitude","leftSigma","rightSigma","undershoot","pedestal");
					        fitLLu->SetParameter(0,minRange+20);
					        fitLLu->SetParameter(1,80.);
					        fitLLu->SetParameter(2,10.);
					        fitLLu->SetParameter(3,10.);
					        fitLLu->SetParameter(4,1.);
					        fitLLu->FixParameter(5,pedestal);
					        fitLLu->SetParLimits(0,minRangeLLu,maxRangeLLu);
					        fitLLu->SetParLimits(1,21,1700);
					        fitLLu->SetParLimits(2,5,50);
					        fitLLu->SetParLimits(3,5,50);
					        fitLLu->SetParLimits(4,0,350);
					        fitLLu->SetLineColor(4);

						int LLufitreturn=1; 
					       
					        LLufitreturn= (int) hist->Fit("fitLLu","RQW");
					        histallfitsLLu->Fill(part,maxvalue);

	                        		std::cout<<"LLu fit return is "<<LLufitreturn<<std::endl;
					       
					        double chi2LLu = fitLLu->GetChisquare();
					        int ndfLLu = fitLLu->GetNDF();
					        double *parfitLLu = new double[5];
					        double *pardiffLLu = new double[2];
					        parfitLLu = fitLLu->GetParameters();
					        pardiffLLu[0] = parfitLLu[0]-refpar[0];
					        pardiffLLu[1] = (parfitLLu[1]-refpar[1])/refpar[1];
					       

	                                	std::cout<<"GLu fit performed"<<std::endl;

					        char newHistNameLLu[30];

					        if (chi2LLu/ndfLLu <= maxchi2ndf && LLufitreturn==0){


					        	sprintf(newHistNameLLu,"HistfitLLu0x%08x_success",CoolID);
					        	hist->SetName(newHistNameLLu);
					        	out_file_LLu.cd();
					        	hist->Write();

							char newerHistTitle[30];
							sprintf(newerHistTitle, "fitted hist (sigl = %f sigr = %f chi2/ndf = %f)", parfitLLu[2], parfitLLu[3], chi2LLu/ndfLLu);
							hist->SetTitle(newerHistTitle);

							double DA = parfitLLu[4]/parfitLLu[1];
							if (sigmal_map.count(CoolID)){
								if (chi2ndf_map[CoolID]>(chi2LLu/ndfLLu)){
									sigmal_map[CoolID]=parfitLLu[2];
									sigmar_map[CoolID]=parfitLLu[3];
									us_map[CoolID]=parfitLLu[4];
									DA_map[CoolID]=DA;
									chi2ndf_map[CoolID]=chi2LLu/ndfLLu;
									func_map[CoolID]=2.;
								}
							}
							else {
								sigmal_map[CoolID]=parfitLLu[2];
								sigmar_map[CoolID]=parfitLLu[3];
								us_map[CoolID]=parfitLLu[4];
								DA_map[CoolID]=DA;
								chi2ndf_map[CoolID]=chi2LLu/ndfLLu;
								func_map[CoolID]=2.;
							}


					                histmaxposLLu->Fill(pardiffLLu[0]);
					                histampLLu->Fill(pardiffLLu[1]);
					                histchi2LLu->Fill(chi2LLu/ndfLLu);
					                if (layer==0){
					                        emlaychi2LLu->Fill(eta,phi,chi2LLu/ndfLLu);
					                        emlaymaxposLLu->Fill(eta,phi,pardiffLLu[0]+1000);
					                        emlayamplLLu->Fill(eta,phi,pardiffLLu[1]+1000);
					                        emlaysigm1LLu->Fill(eta,phi,parfitLLu[2]);
					                        emlaysigm2LLu->Fill(eta,phi,parfitLLu[3]);
					                }
					                else if (layer==1){
					                        hadlaychi2LLu->Fill(eta,phi,chi2LLu/ndfLLu);
					                        hadlaymaxposLLu->Fill(eta,phi,pardiffLLu[0]+1000);
					                        hadlayamplLLu->Fill(eta,phi,pardiffLLu[1]+1000);
					                        hadlaysigm1LLu->Fill(eta,phi,parfitLLu[2]);
					                        hadlaysigm2LLu->Fill(eta,phi,parfitLLu[3]);
					                }
					               
					                if ( part == 3 ){ //EMB
					                        histundEMBLLu->Fill(parfitLLu[4]/parfitLLu[1]);
					                }
					                if ( part == 1 ){ //EMEC
					                        if ( -2.5 <= eta && eta <= 2.5 ) histundEMECLLu1->Fill(parfitLLu[4]/parfitLLu[1]);
					                        else histundEMECLLu2->Fill(parfitLLu[4]/parfitLLu[1]);
					                }
					                if ( part == 6 ){ //HEC
					                        histundHECLLu->Fill(parfitLLu[4]/parfitLLu[1]);
					                }
					                if ( part == 0 || part == 4 || part == 5){ //FCal
					                        histundFCalLLu->Fill(parfitLLu[4]/parfitLLu[1]);
					                }
					                if (part == 7 || part == 8 ){ //Tile
					                        histundTileLLu->Fill(parfitLLu[4]/parfitLLu[1]);
					                }
				               
				       		 } // close of passed LLu fit
				                else {
					        	sprintf(newHistNameLLu,"HistfitLLu0x%08x_failed",CoolID);
					        	hist->SetName(newHistNameLLu);
					        	out_file_LLu.cd();
					        	hist->Write();
				                        nfailfitLLu++;
				                        histfitfailLLu->Fill(part,maxvalue);
				                }
					}
//                              else std::cout<<histName<<"\tLandau-Landau fit w.u. sm\tchi2/ndf=\t"<<chi2LLu/ndfLLu<<std::endl;
                        } // close of # 4 (ref fit passed)
                        else {
                                nfailfitref++;
                                histfitfailRef->Fill(part,maxvalue);
                                std::cout<<"failed reference fit for channel:\t"<<histName<<"\tchi2/ndf\t"<<refchi2/refndf<<"\t--> maxbin/maxvalue\t"<<maxbin<<"/"<<maxvalue<<std::endl;
                        }
                //--------------------------------------------------------------------------------
                } //close of # 3 (no low cnts or bad)
                else {
                        bad = false;
                }
        } // close of # 2 (hist exists)
        hist = 0;
	} // close of # 1 (loop through id)

//}}}
 
        // fit statistics
        std::cout<<"-----Fit statistics:-----------"<<std::endl;
        std::cout<<"number of saturated pulses:\t\t\t"<<nsat<<std::endl;
        std::cout<<"number of tts with no pulses:\t\t\t"<<nnopulse<<std::endl;
        std::cout<<"number of applied reference fits:\t\t"<<nfitref<<std::endl;
        std::cout<<"number of failed reference fits:\t\t"<<nfailfitref<<std::endl;
        std::cout<<"-----------------------------\nnumber of applied fits per fitfunction\n(GLu or LLu):\t\t\t\t\t"<<nfit<<std::endl;
        std::cout<<"fitfunction\t\t\t\t\tnumber of failed fits"<<std::endl;
        std::cout<<"Gauss-Landau w.u. small range (GLu)\t\t"<<nfailfitGLu<<std::endl;
        std::cout<<"Landau-Landau w.u. small range (LLu)\t\t"<<nfailfitLLu<<std::endl;
        std::cout<<"------------------------------"<<std::endl;
       
               
//-------------------------------------------------------------------------------------------------
   
        //TCanvas *c = new TCanvas("histamplcmb","Histogram amplitudes combined",700,500);
        //c->SetFillColor(0);
       
        double maxAmp[3];
        maxAmp[0] = histamplRef->GetMaximum();
        maxAmp[1] = histampGLu->GetMaximum();
        maxAmp[2] = histampLLu->GetMaximum();
        double maxampl = TMath::MaxElement(3,maxAmp);
       
        histamplRef->GetYaxis()->SetRangeUser(0,maxampl*1.1);
        histamplRef->SetStats(0);
//      histamplRef->SetLineColor(0);
        histamplRef->SetTitle(0);
        histamplRef->Draw();
        histampGLu->SetLineColor(2);
        histampGLu->Draw("same");
        histampLLu->SetLineColor(4);
        histampLLu->Draw("same");
       
/*        TLegend* leg1 = new TLegend(0.63, 0.73, 0.93, 0.93);
        leg1->SetFillColor(0);
        leg1->AddEntry(histamplRef,"Reference fit - LL for HEC, GL otherwise","lpf");
        leg1->AddEntry(histampGLu,"GLu fit","lpf");
        leg1->AddEntry(histampLLu,"LLu fit","lpf");
        leg1->Draw();

        TCanvas *d = new TCanvas("histmaxposcmb","Histogram maximum positions combined",700,500);
        d->SetFillColor(0); */
       
        double maxMPos[3];
        maxMPos[0] = histmaxposRef->GetMaximum();
        maxMPos[1] = histmaxposGLu->GetMaximum();
        maxMPos[2] = histmaxposLLu->GetMaximum();
        double maxmpos = TMath::MaxElement(3,maxMPos);
       
        histmaxposRef->GetYaxis()->SetRangeUser(0,maxmpos*1.1);
        histmaxposRef->SetStats(0);
//      histmaxposRef->SetLineColor(0);
        histmaxposRef->SetTitle(0);
        histmaxposRef->Draw();
        histmaxposGLu->SetLineColor(2);
        histmaxposGLu->Draw("same");
        histmaxposLLu->SetLineColor(4);
        histmaxposLLu->Draw("same");
       
 /*       TLegend* leg2 = new TLegend(0.63, 0.73, 0.93, 0.93);
        leg2->SetFillColor(0);
        leg2->AddEntry(histmaxposRef,"Reference fit - LL for HEC, GL otherwise", "lpf");
        leg2->AddEntry(histmaxposGLu,"GLu fit","lpf");
        leg2->AddEntry(histmaxposLLu,"LLu fit","lpf");
        leg2->Draw();
*/   
       
        //Set plotting range
        double max = 0;
        double min = 0;
       
        max = emlaymaxposRef->GetMaximum();
        min = emlaymaxposRef->GetMinimum(-1000);
        emlaymaxposRef->GetZaxis()->SetRangeUser(min*1.1,max*1.1);
        max = hadlaymaxposRef->GetMaximum();
        min = hadlaymaxposRef->GetMinimum(-1000);
        hadlaymaxposRef->GetZaxis()->SetRangeUser(min*1.1,max*1.1);
   	max = emlayamplRef->GetMaximum();
        min = emlayamplRef->GetMinimum(-1000);
        emlayamplRef->GetZaxis()->SetRangeUser(min*1.1,max*1.1);
        max = hadlayamplRef->GetMaximum();
        min = hadlayamplRef->GetMinimum(-1000);
        hadlayamplRef->GetZaxis()->SetRangeUser(min*1.1,max*1.1);

        max = emlaymaxposGLu->GetMaximum();
        min = emlaymaxposGLu->GetMinimum(-1000);
        emlaymaxposGLu->GetZaxis()->SetRangeUser(min*1.1,max*1.1);
        max = hadlaymaxposGLu->GetMaximum();
        min = hadlaymaxposGLu->GetMinimum(-1000);
        hadlaymaxposGLu->GetZaxis()->SetRangeUser(min*1.1,max*1.1);
  	max = emlayamplGLu->GetMaximum();
        min = emlayamplGLu->GetMinimum(-1000);
        emlayamplGLu->GetZaxis()->SetRangeUser(min*1.1,max*1.1);
        max = hadlayamplGLu->GetMaximum();
        min = hadlayamplGLu->GetMinimum(-1000);
        hadlayamplGLu->GetZaxis()->SetRangeUser(min*1.1,max*1.1);
       
        max = emlaymaxposLLu->GetMaximum();
        min = emlaymaxposLLu->GetMinimum(-1000);
        emlaymaxposLLu->GetZaxis()->SetRangeUser(min*1.1,max*1.1);
        max = hadlaymaxposLLu->GetMaximum();
        min = hadlaymaxposLLu->GetMinimum(-1000);
        hadlaymaxposLLu->GetZaxis()->SetRangeUser(min*1.1,max*1.1);
   	max = emlayamplLLu->GetMaximum();
        min = emlayamplLLu->GetMinimum(-1000);
        emlayamplLLu->GetZaxis()->SetRangeUser(min*1.1,max*1.1);
        max = hadlayamplLLu->GetMaximum();
        min = hadlayamplLLu->GetMinimum(-1000);
        hadlayamplLLu->GetZaxis()->SetRangeUser(min*1.1,max*1.1);
       
        TH2 *tmp = new TH2D("tmp","temporary histogram",9,-0.5,8.5,64,-0.5,1023.5);
        tmp->Add(histallfitsRef,histfitfailRef,1,-1);
        histQsuccessFitRef->Divide(tmp,histallfitsRef,1,1);
        tmp->Reset();
        tmp->Add(histallfitsGLu,histfitfailGLu,1,-1);
        histQsuccessFitGLu->Divide(tmp,histallfitsGLu,1,1);
        tmp->Reset();
        tmp->Add(histallfitsLLu,histfitfailLLu,1,-1);
        histQsuccessFitLLu->Divide(tmp,histallfitsLLu,1,1);
        delete tmp;

        out_file_fitquality.cd();
        //Write to file

//      std::cout<<"writing histograms to file"<<std::endl;

        histundEMBGLu->Write();
        histundEMECGLu1->Write();
        histundEMECGLu2->Write();
        histundHECGLu->Write();
        histundFCalGLu->Write();
        histundTileGLu->Write();
        histundEMBLLu->Write();
        histundEMECLLu1->Write();
        histundEMECLLu2->Write();
        histundHECLLu->Write();
        histundFCalLLu->Write();
        histundTileLLu->Write();
       
        histmaxposRef->Write();
        histamplRef->Write();
        histchi2Ref->Write();
       
        histmaxposGLu->Write();
   	histampGLu->Write();
        histchi2GLu->Write();

        histmaxposLLu->Write();
   	histampLLu->Write();
        histchi2LLu->Write();
       
        histallfitsRef->SetOption("colz");
        histfitfailRef->SetOption("colz");
        histQsuccessFitRef->SetOption("colz");
        emlaychi2Ref->SetOption("colz");
  	 hadlaychi2Ref->SetOption("colz");
        emlaymaxposRef->SetOption("colz");
        hadlaymaxposRef->SetOption("colz");
        emlayamplRef->SetOption("colz");
        hadlayamplRef->SetOption("colz");

        emlay_overview->SetOption("colz");
        hadlay_overview->SetOption("colz");
        histallfitsGLu->SetOption("colz");
        histfitfailGLu->SetOption("colz");
        histQsuccessFitGLu->SetOption("colz");
        emlaychi2GLu->SetOption("colz");
   	hadlaychi2GLu->SetOption("colz");
        emlaymaxposGLu->SetOption("colz");
        hadlaymaxposGLu->SetOption("colz");
        emlayamplGLu->SetOption("colz");
        hadlayamplGLu->SetOption("colz");
        emlaysigm1GLu->SetOption("colz");
        hadlaysigm1GLu->SetOption("colz");
        emlaysigm2GLu->SetOption("colz");
        hadlaysigm2GLu->SetOption("colz");
       
        histallfitsLLu->SetOption("colz");
        histfitfailLLu->SetOption("colz");
        histQsuccessFitLLu->SetOption("colz");
        emlaychi2LLu->SetOption("colz");
   	hadlaychi2LLu->SetOption("colz");
        emlaymaxposLLu->SetOption("colz");
        hadlaymaxposLLu->SetOption("colz");
        emlayamplLLu->SetOption("colz");
        hadlayamplLLu->SetOption("colz");
        emlaysigm1LLu->SetOption("colz");
        hadlaysigm1LLu->SetOption("colz");
        emlaysigm2LLu->SetOption("colz");
        hadlaysigm2LLu->SetOption("colz");

	emlay_overview->Write();
	hadlay_overview->Write();

        histallfitsRef->Write();
        histfitfailRef->Write();
        histQsuccessFitRef->Write();
        emlaychi2Ref->Write();
   	hadlaychi2Ref->Write();
        emlaymaxposRef->Write();
        hadlaymaxposRef->Write();
        emlayamplRef->Write();
        hadlayamplRef->Write();

        histallfitsGLu->Write();
        histfitfailGLu->Write();
        histQsuccessFitGLu->Write();
        emlaychi2GLu->Write();
   	hadlaychi2GLu->Write();
        emlaymaxposGLu->Write();
        hadlaymaxposGLu->Write();
        emlayamplGLu->Write();
        hadlayamplGLu->Write();
        emlaysigm1GLu->Write();
        hadlaysigm1GLu->Write();
        emlaysigm2GLu->Write();
        hadlaysigm2GLu->Write();
       
        histallfitsLLu->Write();
        histfitfailLLu->Write();
        histQsuccessFitLLu->Write();
        emlaychi2LLu->Write();
   	hadlaychi2LLu->Write();
        emlaymaxposLLu->Write();
        hadlaymaxposLLu->Write();
        emlayamplLLu->Write();
        hadlayamplLLu->Write();
        emlaysigm1LLu->Write();
        hadlaysigm1LLu->Write();
        emlaysigm2LLu->Write();
        hadlaysigm2LLu->Write();

	TTxml m_par_replace;
  	m_par_replace.Add("sigmal", sigmal_map);
  	m_par_replace.Add("sigmar", sigmar_map);
  	m_par_replace.Add("us", us_map);
  	m_par_replace.Add("undVsAmpl", DA_map);
  	m_par_replace.Add("chi2ndf", chi2ndf_map);
  	m_par_replace.Add("func", func_map);
  	m_par_replace.Add("part", part_map);
  	m_par_replace.Add("eta", eta_map);
  	m_par_replace.Add("phi", phi_map);
  	m_par_replace.Add("pedestal", ped_map);

	if (ifile) {
		if( remove( "/afs/cern.ch/work/c/cantel/private/output/TTparameters.xml" ) != 0 )
		perror( "Error deleting file" );
  		else puts( "File successfully deleted" );
	}
	else puts("TTpar file does not yet exist. Creating new file.");
	
  	m_par_replace.Write("/afs/cern.ch/work/c/cantel/private/output/TTparameters.xml");
       
        //c->Write();
        //d->Write();
       
//      std::cout<<"end of writing process"<<std::endl;
   
	out_file_ref.Close();
	out_file_GLu.Close();
	out_file_LLu.Close();
	out_file_fitquality.Close();
	m_output.close();

} // close of function fitrun

///////////////////////////////////////////////////////////////////////////////////////
/////////////                                                            //////////////
/////////////    int main()                                              //////////////
/////////////                                                            //////////////
///////////////////////////////////////////////////////////////////////////////////////

/*
int main(int argc, const char *argv[]){

        std::cout << "-------------------------------------------" << std::endl;
        std::cout << "Meta-Data of PHOS4 scan fits" << std::endl;
        std::cout << "-------------------------------------------" << std::endl;

        std::string filepath;
        filepath.assign(argv[1]);

        size_t found;
 
        std::string runNumber;
        found = filepath.find_last_of("/");
        runNumber = filepath.substr(found+4,6);
        int int_runNo = 0;
        sscanf(runNumber.c_str(),"%d",&int_runNo);

        std::cout << "input & output\nfilepath\t" << filepath << std::endl;
        std::cout << "runNumber\t" << int_runNo << std::endl;

        char inputfile[150];
        sprintf(inputfile,"%s/Phos4scan%s_pedestalcorrected.root",filepath.c_str(),runNumber.c_str());
        std::cout << "inputfile\t" << inputfile << std::endl;

        char outputfile_ref[150];
        char outputfile_GLu[150];
        char outputfile_LLu[150];
        char outputfile_fitquality[150];
        sprintf(outputfile_ref,"%s/Phos4scan%s_fitRef.root",filepath.c_str(),runNumber.c_str());
        sprintf(outputfile_GLu,"%s/Phos4scan%s_fitGLu.root",filepath.c_str(),runNumber.c_str());
        sprintf(outputfile_LLu,"%s/Phos4scan%s_fitLLu.root",filepath.c_str(),runNumber.c_str());
        sprintf(outputfile_fitquality,"%s/Phos4scan%s_fitquality.root",filepath.c_str(),runNumber.c_str());
        std::cout << "outputfiles\t" << outputfile_ref << "\n\t\t" << outputfile_GLu << "\n\t\t" << outputfile_LLu << "\n\t\t" << outputfile_fitquality << "\n\t\t" << std::endl;

//      int pedestal = 0;
//      sscanf(argv[2],"%*9s%d",&pedestal);
//      std::cout << "pedestal\t" << pedestal << std::endl;

        std::cout << "-------------------------------------------" << std::endl;
        std::cout << "Start fitting PHOS4 scan " << std::endl;
        std::cout << "-------------------------------------------" << std::endl;

        fitPulserRun();//int_runNo,inputfile,outputfile_ref,outputfile_GLu,outputfile_LLu,outputfile_fitquality);
        std::cout << "Fitting PHOS4 scan finished" << std::endl;

        return 0;
} */ // close of main()

