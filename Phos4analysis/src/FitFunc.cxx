#include "FitFunc.h"
#include <algorithm>
#include <iostream>

FitFunc::FitFunc() : fithist(0), func(0), m_canvas(0), m_line(0), m_tmax(60), m_amp(400), m_iscanvas(false) 
{

}

FitFunc::~FitFunc()
{
/*	if( fithist != 0 )
	{
		delete fithist;
		fithist = 0;
	}*/
	if( func != 0 )
	{
		delete func;
		func = 0;
	}
/*	if( m_canvas != 0 )
	{
		delete m_canvas;
		m_canvas = 0;
	}*/

}

double* FitFunc::set_parameter_us( int crate_number )
{
	double* par = new double[6];
	par[0] = m_tmax;
	par[1] = m_amp;
	par[2] = 0.9*25;
	par[4] = 32;
	par[5] = 100;

	switch( crate_number )
	{
		case 0:
			par[3] = 1.1*25;
			break;
		case 1:
			par[3] = 1.1*25;
			break;
		case 2:
			par[3] = 1.1*25;
			break;
		case 3:
			par[3] = 1.1*25;
			break;
		case 4:
			par[3] = 0.6*25;
			break;
		case 5:
			par[3] = 0.6*25;
			break;
		case 6:
			par[3] = 0.6*25;
			break;
		case 7:
			par[3] = 0.6*25;
			break;
	}

	return par;
}

double* FitFunc::set_parameter_usF( int crate_number )
{
	double* par = new double[6];
	par[0] = m_tmax;
	par[1] = m_amp;
	par[2] = 0.9*25;
	par[4] = 32;
	par[5] = 1;

	switch( crate_number )
	{
		case 0:
			par[3] = 1.1*25;
			break;
		case 1:
			par[3] = 1.1*25;
			break;
		case 2:
			par[3] = 1.1*25;
			break;
		case 3:
			par[3] = 1.1*25;
			break;
		case 4:
			par[3] = 0.6*25;
			break;
		case 5:
			par[3] = 0.6*25;
			break;
		case 6:
			par[3] = 0.6*25;
			break;
		case 7:
			par[3] = 0.6*25;
			break;
	}

	return par;
}

double* FitFunc::set_parameter( int crate_number )
{
	double* par = new double[5];
	par[0] = m_tmax;
	par[1] = m_amp;
	par[2] = 0.9*25;
	par[4] = 32;

	switch( crate_number )
	{
		case 0:
			par[3] = 1.1*25;
			break;
		case 1:
			par[3] = 1.1*25;
			break;
		case 2:
			par[3] = 1.1*25;
			break;
		case 3:
			par[3] = 1.1*25;
			break;
		case 4:
			par[3] = 0.6*25;
			break;
		case 5:
			par[3] = 0.6*25;
			break;
		case 6:
			par[3] = 0.6*25;
			break;
		case 7:
			par[3] = 0.6*25;
			break;
	}

	return par;
}


double FitFunc::Fit_GLu(double *x, double *par) {
	      double xmax = par[0];
	      double ampl = par[1];
	      double sigma_g = par[2];
	      double sigma_l = par[3];
	      double pedestal =  par[4];
	      double undershoot = par[5];
	      
	     
	      if ( x[0] <= par[0]){
	        //Gauss function
	        return( ampl*exp(-0.5*(x[0]-xmax)*(x[0]-xmax)/(sigma_g*sigma_g)-0.5)+pedestal);
	      }
	      if (x[0] > par[0]){
	        //Landau function
	        return( (ampl+undershoot*exp(0.5))*exp(-0.5*((x[0]-xmax)/sigma_l + exp(-(x[0]-xmax)/sigma_l)))+pedestal-undershoot);
	      }
	   
	      else return -1;
	}

double FitFunc::Fit_GLuF(double *x, double *par) {
	      double xmax = par[0];
	      double ampl = par[1];
	      double sigma_g = par[2];
	      double sigma_l = par[3];
	      double pedestal =  par[4];
	      double undershoot = par[5]*ampl;
	      
	     
	      if ( x[0] <= par[0]){
	        //Gauss function
	        return( ampl*exp(-0.5*(x[0]-xmax)*(x[0]-xmax)/(sigma_g*sigma_g)-0.5)+pedestal);
	      }
	      if (x[0] > par[0]){
	        //Landau function
	        return( (ampl+undershoot*exp(0.5))*exp(-0.5*((x[0]-xmax)/sigma_l + exp(-(x[0]-xmax)/sigma_l)))+pedestal-undershoot);
	      }
	   
	      else return -1;
	}

double FitFunc::Fit_LLu(double *x, double *par) {
	      double xmax = par[0];
	      double ampl = par[1];
	      double sigma_l1 = par[2];
	      double sigma_l2 = par[3];
	      double pedestal = par[4];
	      double undershoot = par[5];
	     
	      if ( x[0] <= par[0]){
	        //Landau function
	        return(ampl*exp(-0.5*((x[0]-xmax)/sigma_l1 + exp(-(x[0]-xmax)/sigma_l1))) +pedestal);
	      }
	      if (x[0] > par[0]){
	        //Landau function with undershoot
	        return( (ampl+undershoot*exp(0.5))*exp(-0.5*((x[0]-xmax)/sigma_l2 + exp(-(x[0]-xmax)/sigma_l2)))+pedestal-undershoot);
	      }
	   
	      else return -1;
	}

double FitFunc::Fit_LLuF(double *x, double *par) {
	      double xmax = par[0];
	      double ampl = par[1];
	      double sigma_l1 = par[2];
	      double sigma_l2 = par[3];
	      double pedestal = par[4];
	      double undershoot = par[5]*ampl;
	     
	      if ( x[0] <= par[0]){
	        //Landau function
	        return(ampl*exp(-0.5*((x[0]-xmax)/sigma_l1 + exp(-(x[0]-xmax)/sigma_l1))) +pedestal);
	      }
	      if (x[0] > par[0]){
	        //Landau function with undershoot
	        return( (ampl+undershoot*exp(0.5))*exp(-0.5*((x[0]-xmax)/sigma_l2 + exp(-(x[0]-xmax)/sigma_l2)))+pedestal-undershoot);
	      }
	   
	      else return -1;
	}

std::vector<int> FitFunc::fill_tvector( int length ) // length = 5
	{
	        std::vector<int> tvalues;
	        for( int i=0; i<length; ++i )
	        {
	                tvalues.push_back( i );
	        }
	
	        return tvalues;
	}

std::vector<int> FitFunc::fill_vector( double *par, int length )  // length = 5
{
	std::vector<int> tvalues = FitFunc::fill_tvector( length );
	std::vector<int> yvalues;
	
	
	TF1* tf1 = new TF1( "tf1", this, &FitFunc::Fit_GLu, tvalues.front(), tvalues.back(), 5 , "FitFunc", "gauss_landau5" );
//	TF1* tf1 = new TF1( "tf1", gauss_landau5, 0, 125, 5 );
	tf1->SetParameters( par[0], par[1], par[2], par[3], par[4] ); 
	
	for( int i=0; i<length; ++i )
	{
		yvalues.push_back( (int) tf1->Eval( tvalues[i] ));
	}
/*
	for( int i=0; i<5; ++i ) cout << tvalues[i] << "\t";
	cout << endl;
	for( int i=0; i<5; ++i ) cout << yvalues[i] << "\t";
	cout << endl;
*/

	delete tf1;
	tf1 = 0;
	return yvalues;

}

std::vector<int> FitFunc::fill_vector_glu( double *par, int length )  // length = 5
{
	std::vector<int> tvalues = FitFunc::fill_tvector( length );
	std::vector<int> yvalues;
	
	
	TF1* tf1 = new TF1( "tf1", this, &FitFunc::Fit_GLu, tvalues.front(), tvalues.back(), 6 , "FitFunc", "GLu" );
//	TF1* tf1 = new TF1( "tf1", gauss_landau5, 0, 125, 5 );
	tf1->SetParameters( par );
	
	for( int i=0; i<length; ++i )
	{
		yvalues.push_back( (int) tf1->Eval( tvalues[i] ));
	}
/*
	for( int i=0; i<5; ++i ) cout << tvalues[i] << "\t";
	cout << endl;
	for( int i=0; i<5; ++i ) cout << yvalues[i] << "\t";
	cout << endl;
*/

	delete tf1;
	tf1 = 0;
	return yvalues;

}

std::vector<int> FitFunc::fill_vector_llu( double *par, int length )  // length = 5
{
	std::vector<int> tvalues = FitFunc::fill_tvector( length );
	std::vector<int> yvalues;
	
	
	TF1* tf1 = new TF1( "tf1", this, &FitFunc::Fit_LLu, tvalues.front(), tvalues.back(), 6 , "FitFunc", "LLu" );
//	TF1* tf1 = new TF1( "tf1", gauss_landau5, 0, 125, 5 );
	tf1->SetParameters( par );
	
	for( int i=0; i<length; ++i )
	{
		yvalues.push_back( (int) tf1->Eval( tvalues[i] ));
	}
/*
	for( int i=0; i<5; ++i ) cout << tvalues[i] << "\t";
	cout << endl;
	for( int i=0; i<5; ++i ) cout << yvalues[i] << "\t";
	cout << endl;
*/

	delete tf1;
	tf1 = 0;
	return yvalues;
}

TH1D* FitFunc::fill_histo( std::vector<int> tval, std::vector<int> yval, std::string hname ) 	// hname = "hff"
{
	int size = tval.size();
	if (size != yval.size())
	{
		std::cout << "sizes of vectors don't match --- good bye!" << std::endl;
		return 0;
	}
	
	TH1D* h1 = new TH1D( hname.c_str(), "FitFunc histo", size, tval.front(), tval.back()); 
	
	for ( int i=0; i<size; ++i )
	{
		h1->Fill( tval[i], yval[i] );
	}

	return h1;
	}

std::vector<int> FitFunc::read_histo( TH1* fithist )
{
	std::vector<int> vec;
	int nbins = fithist->GetNbinsX();
	for( int i=1; i<=nbins; ++i )
	{
		vec.push_back( (int)fithist->GetBinContent( i ) );
	}
	
	return vec;
}

int FitFunc::do_gof( std::vector<int> yvalues, int tpeak, int crate_number )
//double FitFunc::do_gof( int ymidbin, double tmax, double amp )
{
	int gof;
	//check on position of fit maximum
	if( m_tmax >= tpeak-25. && m_tmax <= tpeak+12.5 )  gof = 3000;
	else if( m_tmax >= tpeak-37.5 && m_tmax <= tpeak+25. ) 
	{
		gof = 2000;
//		std::cout << "bad tmax value" << std::endl;
	}
	else
	{
		gof= 1000; 
//		return gof;
	}

	std::vector<int>::iterator it_max;
	it_max = std::max_element( yvalues.begin(), yvalues.end() );
	//check on size of fit amplitude
//	double amp_red = amp * exp( -0.5 );
	if( 0.85* *it_max <= m_amp && 1.15* *it_max >=m_amp ) gof += 300;
	else if(( 0.65* *it_max<=m_amp && 1.35* *it_max >= m_amp ))
	{	
		gof += 200;
//		std::cout << "bad amplitude" << std::endl;
	}	
	else
	{
		gof+= 100; 
//		return gof;
	}
	
	double *par = FitFunc::set_parameter( crate_number );
	par[0] = m_tmax - tpeak + 50.;
	par[1] = ( m_amp - par[4] ) * exp( 0.5 );
	//changes: new name for y values of Gauss-Landau function
	std::vector<int> yfit = FitFunc::fill_vector( par, 5 );
	
	//changes: new variables
	double ymean0 = ( it_max[-2] + yfit[0] )/2.;
	double ydiff0 = abs( it_max[-2] - yfit[0] );
	double ymean4 = ( it_max[2] + yfit[4] )/2.;
	double ydiff4 = abs( it_max[2] - yfit[4] );
	
	//check on deviation at the left tail of the fit
	//changes: new if-conditions and new limit values
	//Please check whether those limits are sensible!!
	if( ydiff0 <= 0.6*ymean0 ) gof += 30;
	else if( ydiff0 <= 1.*ymean0 )
	{
		gof += 20;
//		std::cout << "bad left tail" << std::endl;
	}
	else
	{
		gof+= 10; 
//		return gof;
	}

	//check on deviation at the right tail of the fit
	//changes: new if-conditions and new limit values
	if( ydiff4 <= 0.25*ymean4 ) gof += 3;
	else if( ydiff4 <= 0.4*ymean4 )
	{
		gof += 2;
//		std::cout << "bad right tail" << std::endl;
	}
	else
	{
		gof += 1; 
//		std::cout << "it_max[2]:\t" << it_max[2] << std::endl;
//		std::cout << "yfit[4]:\t" << yfit[4] << std::endl;
//		std::cout << "ymean4:\t" << ymean4 << std::endl;
//		std::cout << "ydiff4:\t" << ydiff4 << std::endl;
//		std::cout << "ydiff4/ymean4:\t" << ydiff4/ymean4 << std::endl << std::endl;
//		return gof;
	}
	
	return gof;	
}

int FitFunc::do_gof_glu( std::vector<int> yvalues, int tpeak, double* parameter )
//double FitFunc::do_gof( int ymidbin, double tmax, double amp )
{
//	std::cout << "par[0]:\t" << parameter[0] << std::endl;
//	std::cout << "par[1]:\t" << parameter[1] << std::endl;
//	std::cout << "par[2]:\t" << parameter[2] << std::endl;
//	std::cout << "par[3]:\t" << parameter[3] << std::endl;
//	std::cout << "par[4]:\t" << parameter[4] << std::endl;
//	std::cout << "par[5]:\t" << parameter[5] << std::endl << std::endl;

	int gof;
	//check on position of fit maximum
	
	std::vector<int>::iterator it_max;
	it_max = std::max_element( yvalues.begin(), yvalues.end() );

	if( it_max[-1] > it_max[1] )
	{
		if( m_tmax >= tpeak-25. && m_tmax <= tpeak+12.5 )  gof = 3000;
		else if( m_tmax >= tpeak-37.5 && m_tmax <= tpeak+25. ) 
		{
			gof = 2000;
//			std::cout << "bad tmax value" << std::endl;
		}
		else
		{
			gof= 1000; 
//			return gof;
		}
	}
	else
	{

		if( m_tmax >= tpeak-12.5 && m_tmax <= tpeak+25. )  gof = 3000;
		else if( m_tmax >= tpeak-25. && m_tmax <= tpeak+37.5 ) 
		{
			gof = 2000;
//			std::cout << "bad tmax value" << std::endl;
		}
		else
		{
			gof= 1000; 
//			return gof;
		}
	}
	//check on size of fit amplitude
//	double amp_red = amp * exp( -0.5 );
	if( 0.85* *it_max <= m_amp && 1.15* *it_max >=m_amp ) gof += 300;
	else if(( 0.65* *it_max<=m_amp && 1.35* *it_max >= m_amp ))
	{	
		gof += 200;
//		std::cout << "bad amplitude" << std::endl;
	}	
	else
	{
		gof+= 100; 
//		return gof;
	}
	
	std::vector<int> yfit = FitFunc::fill_vector_glu( parameter, yvalues.size() );
	
	int lbin = it_max - yvalues.begin() - 2;
	int rbin = it_max - yvalues.begin() + 2;
	
	//changes: new variables
	double ymean0 = ( it_max[-2] + yfit[lbin] )/2.;
	double ydiff0 = abs( it_max[-2] - yfit[lbin] );
	double ymean4 = ( it_max[2] + yfit[rbin] )/2.;
	double ydiff4 = abs( it_max[2] - yfit[rbin] );
	
	//check on deviation at the left tail of the fit
	//changes: new if-conditions and new limit values
	//Please check whether those limits are sensible!!
	if( ydiff0 <= 0.6*ymean0 ) gof += 30;
	else if( ydiff0 <= 1.*ymean0 )
	{
		gof += 20;
//		std::cout << "bad left tail" << std::endl;
	}
	else
	{
		gof+= 10; 
//		return gof;
	}

	//check on deviation at the right tail of the fit
	//changes: new if-conditions and new limit values
	if( ydiff4 <= 0.25*ymean4 ) gof += 3;
	else if( ydiff4 <= 0.4*ymean4 )
	{
		gof += 2;
//		std::cout << "bad right tail" << std::endl;
	}
	else
	{
		gof += 1; 
//		return gof;
	}
	
//	std::cout << "it_max[2]:\t" << it_max[2] << std::endl;
//	std::cout << "yfit[rbin]:\t" << yfit[rbin] << std::endl;
//	std::cout << "ymean4:\t" << ymean4 << std::endl;
//	std::cout << "ydiff4:\t" << ydiff4 << std::endl;
//	std::cout << "ydiff4/ymean4:\t" << ydiff4/ymean4 << std::endl << std::endl;
	return gof;	
}

int FitFunc::do_gof_llu( std::vector<int> yvalues, int tpeak, double* parameter )
//double FitFunc::do_gof( int ymidbin, double tmax, double amp )
{
//	std::cout << "par[0]:\t" << parameter[0] << std::endl;
//	std::cout << "par[1]:\t" << parameter[1] << std::endl;
//	std::cout << "par[2]:\t" << parameter[2] << std::endl;
//	std::cout << "par[3]:\t" << parameter[3] << std::endl;
//	std::cout << "par[4]:\t" << parameter[4] << std::endl;
//	std::cout << "par[5]:\t" << parameter[5] << std::endl << std::endl;

	int gof;
	//check on position of fit maximum
	
	std::vector<int>::iterator it_max;
	it_max = std::max_element( yvalues.begin(), yvalues.end() );

	if( it_max[-1] > it_max[1] )
	{
		if( m_tmax >= tpeak-25. && m_tmax <= tpeak+12.5 )  gof = 3000;
		else if( m_tmax >= tpeak-37.5 && m_tmax <= tpeak+25. ) 
		{
			gof = 2000;
//			std::cout << "bad tmax value" << std::endl;
		}
		else
		{
			gof= 1000; 
//			return gof;
		}
	}
	else
	{

		if( m_tmax >= tpeak-12.5 && m_tmax <= tpeak+25. )  gof = 3000;
		else if( m_tmax >= tpeak-25. && m_tmax <= tpeak+37.5 ) 
		{
			gof = 2000;
//			std::cout << "bad tmax value" << std::endl;
		}
		else
		{
			gof= 1000; 
//			return gof;
		}
	}
	//check on size of fit amplitude
//	double amp_red = amp * exp( -0.5 );
	if( 0.85* *it_max <= m_amp && 1.15* *it_max >=m_amp ) gof += 300;
	else if(( 0.65* *it_max<=m_amp && 1.35* *it_max >= m_amp ))
	{	
		gof += 200;
//		std::cout << "bad amplitude" << std::endl;
	}	
	else
	{
		gof+= 100; 
//		return gof;
	}
	
	std::vector<int> yfit = FitFunc::fill_vector_llu( parameter, yvalues.size() );
	
	int lbin = it_max - yvalues.begin() - 2;
	int rbin = it_max - yvalues.begin() + 2;
	
	//changes: new variables
	double ymean0 = ( it_max[-2] + yfit[lbin] )/2.;
	double ydiff0 = abs( it_max[-2] - yfit[lbin] );
	double ymean4 = ( it_max[2] + yfit[rbin] )/2.;
	double ydiff4 = abs( it_max[2] - yfit[rbin] );
	
	//check on deviation at the left tail of the fit
	//changes: new if-conditions and new limit values
	//Please check whether those limits are sensible!!
	if( ydiff0 <= 0.6*ymean0 ) gof += 30;
	else if( ydiff0 <= 1.*ymean0 )
	{
		gof += 20;
//		std::cout << "bad left tail" << std::endl;
	}
	else
	{
		gof+= 10; 
//		return gof;
	}

	//check on deviation at the right tail of the fit
	//changes: new if-conditions and new limit values
	if( ydiff4 <= 0.25*ymean4 ) gof += 3;
	else if( ydiff4 <= 0.4*ymean4 )
	{
		gof += 2;
//		std::cout << "bad right tail" << std::endl;
	}
	else
	{
		gof += 1; 
//		return gof;
	}
	
//	std::cout << "it_max[2]:\t" << it_max[2] << std::endl;
//	std::cout << "yfit[rbin]:\t" << yfit[rbin] << std::endl;
//	std::cout << "ymean4:\t" << ymean4 << std::endl;
//	std::cout << "ydiff4:\t" << ydiff4 << std::endl;
//	std::cout << "ydiff4/ymean4:\t" << ydiff4/ymean4 << std::endl << std::endl;
	return gof;	
}


void FitFunc::set_histo_color( TH1* fithist, double gof )
{
	fithist->SetFillColor( 0 );
	
	if( gof < 1. )
	{
		fithist->SetFillColor( 5 );
	}
	if( gof < 0. )
	{
		fithist->SetFillColor( 2 );
	}
	return;
}

double FitFunc::do_simple_gof( int goflong )
{
	double gof = 1.;
	int gofarray[4];
	gofarray[0] = goflong/1000;
	goflong = goflong - gofarray[0]*1000 ;
	gofarray[1] = goflong/100;
	goflong = goflong - gofarray[1]*100;
	gofarray[2] = goflong/10;
	goflong = goflong - gofarray[2]*10;
	gofarray[3] = goflong;

	for( int i=0; i<4; ++i )
	{
		if( gofarray[i] == 1 )
		{
			gof = -1;
			return gof;
		}
		gof = gof * 0.5 * ( gofarray[i] -1 );
	}
	return gof;
}


bool FitFunc::do_slice_fit_us(std::vector<int> yvalues, CaloPartEnum part, int &tmax, int &amp, double &gof, std::string hname ) // hname = "hff"
{
	int vsize = yvalues.size();

	std::vector<int> tvalues = fill_tvector( vsize );

	fithist = fill_histo( tvalues, yvalues, hname ); 

	//find fit region
	std::vector<int>::iterator it;
	std::vector<int>::iterator it_max;
	it_max = std::max_element( yvalues.begin(), yvalues.end() );
	int tmax_pos = it_max - yvalues.begin();

	
	int floedge = (int) fithist->GetBinCenter( 1 );
	int fupedge = (int) fithist->GetBinCenter( vsize );
	int lastbin = *it_max;
	
	for( int i=0; it_max+i < yvalues.end(); ++i )
	{
/*		if ((part == LArFCALEM) || (part == LArFCALHAD2) || (part == LArFCALHAD3))
		{
			if( it_max[i] <= 0 )
			{
//			std::cout << "shifting fit range to bin " <<  it_max - yvalues.begin() + i + 1 << std::endl;
			fupedge = (int) hist->GetBinCenter( it_max - yvalues.begin() + i );
			break;
			}
		}
		else 
		{
			if( it_max[i] < -10 )
			{
	//			std::cout << "shifting fit range to bin " <<  it_max - yvalues.begin() + i + 1 << std::endl;
				fupedge = (int) hist->GetBinCenter( it_max - yvalues.begin() + i );
				break;
			}
		}*/
		if( it_max[i] >= lastbin && it_max[i] < 40 )
		{
			//std::cout << "try to exclude undershoot rise -> bin " << it_max - yvalues.begin() + i << std::endl;
			fupedge = (int) FitFunc::round( fithist->GetBinCenter( it_max - yvalues.begin() + i - 1 ) );
			break;
		}
		lastbin = it_max[i];
	}
	double* par = new double[6];
	par[0] = m_tmax;
	par[1] = m_amp;

	TF1* func = setup_fit(part, par, floedge, fupedge);
	if (func)
	{	
		func->SetParameter( 0, tvalues[tmax_pos] );
		func->SetParameter( 1, (*it_max-par[4]) * exp( 0.5 ));

		//m_canvas = new TCanvas( "cff11", "cff11", 800, 600 );
		Int_t fitStatus = fithist->Fit( "fit function", "QR" );
		if (fitStatus == 0)
		{
			m_tmax = (int) func->GetParameter( 0 );
			m_amp = (int) ( func->GetParameter( 1 ) *exp( -0.5 ) + par[4] );
			double* para= func->GetParameters();
	
			int goflong = FitFunc::get_gof( yvalues, tvalues.at( it_max - yvalues.begin() ), para, part);

			gof = do_simple_gof( goflong );
	
			char htitle[64];
			sprintf( htitle, "%d parameters %2f %2f %3f", goflong, para[2], para[3], para[5]);
			fithist->SetTitle( htitle );
	
			fithist->GetYaxis()->SetRangeUser( 0, *it_max*1.2 );

			FitFunc::set_histo_color( fithist, gof );
			//m_canvas->Clear();

			tmax = m_tmax;
			amp = m_amp;
			m_para = para;
			return true; //fit did not fail ROOT fit.
		}
		else {
			gof = -1;
			return false; //failed ROOT fit.
		}  
	}
	else
	{
		gof = -6;
		return false; // undefined detector region.
	}
}

void FitFunc::do_slice_fit_gluF(std::vector<int> yvalues, int crate_number, int &tmax, int &amp, double &gof, std::string hname ) // hname = "hff"
{
	int vsize = yvalues.size();

	std::vector<int> tvalues = fill_tvector( vsize );
	fithist = fill_histo( tvalues, yvalues, hname ); 

	//find fit region
	std::vector<int>::iterator it;
	std::vector<int>::iterator it_max;
	it_max = std::max_element( yvalues.begin(), yvalues.end() );
	int tmax_pos = it_max - yvalues.begin();
	
	// check if fit region is valid, else simply return the hist and set gof=-2
	if( it_max < yvalues.begin()+2 || it_max > yvalues.end()-3 )
	{
		tmax = tvalues[tmax_pos];
		amp = *it_max;
		gof = -2;
		return;
	}

	
	int floedge = (int) fithist->GetBinCenter( 1 );
	int fupedge = (int) fithist->GetBinCenter( vsize );
	int lastbin = *it_max;
	
	for( int i=0; it_max+i < yvalues.end(); ++i )
	{
/*		if( it_max[i] < -10 )
		{
//			std::cout << "shifting fit range to bin " <<  it_max - yvalues.begin() + i + 1 << std::endl;
			fupedge = (int) hist->GetBinCenter( it_max - yvalues.begin() + i );
			break;
		}*/
		if( it_max[i] >= lastbin && it_max[i] < 40 )
		{
			//std::cout << "try to exclude undershoot rise -> bin " << it_max - yvalues.begin() + i << std::endl;
			fupedge = (int) FitFunc::round( fithist->GetBinCenter( it_max - yvalues.begin() + i - 1 ) );
			break;
		}
		lastbin = it_max[i];
	}

	double *parameter = FitFunc::set_parameter_usF( crate_number );
	func = new TF1( "fit function", this, &FitFunc::Fit_GLuF, floedge, fupedge, 6, "FitFunc", "GLuF" );
	func->SetLineColor( 4 );

	func->SetParameter( 0, tvalues[tmax_pos] );
	func->SetParameter( 1, (*it_max-parameter[4]) * exp( 0.5 ));
	func->SetParameter( 2, parameter[2] );
	func->SetParLimits( 2, 1., 30. ); 
	func->SetParLimits( 3, 1., 50. ); 
	func->SetParameter( 3, parameter[3] );
	func->FixParameter( 4, parameter[4] );
	func->SetParameter( 5, parameter[5] );

	//m_canvas = new TCanvas( "cff11", "cff11", 800, 600 );
	fithist->Fit( "fit function", "QR" );
	m_tmax = (int) func->GetParameter( 0 );
	m_amp = (int) ( func->GetParameter( 1 ) *exp( -0.5 ) + parameter[4] );
	double* para= func->GetParameters();
	
	int goflong = FitFunc::do_gof_glu( yvalues, tvalues.at( it_max - yvalues.begin() ), para);
	gof = do_simple_gof( goflong );
	
	char htitle[4];
	sprintf( htitle, "%d", goflong );
	fithist->SetTitle( htitle );
	
	fithist->GetYaxis()->SetRangeUser( 0, *it_max*1.2 );

	FitFunc::set_histo_color( fithist, gof );
	//m_canvas->Clear();

	tmax = m_tmax;
	amp = m_amp;
	m_para = para;

	return;
}

void FitFunc::do_slice_fit_llu(std::vector<int> yvalues, int crate_number, int &tmax, int &amp, double &gof, std::string hname ) // hname = "hff"
{
	int vsize = yvalues.size();

	std::vector<int> tvalues = fill_tvector( vsize );
	fithist = fill_histo( tvalues, yvalues, hname ); 

	//find fit region
	std::vector<int>::iterator it;
	std::vector<int>::iterator it_max;
	it_max = std::max_element( yvalues.begin(), yvalues.end() );
	int tmax_pos = it_max - yvalues.begin();
	
	// check if fit region is valid, else simply return the hist and set gof=-2
	if( it_max < yvalues.begin()+2 || it_max > yvalues.end()-3 )
	{
		tmax = tvalues[tmax_pos];
		amp = *it_max;
		gof = -2;
		return;
	}

	
	int floedge = (int) fithist->GetBinCenter( 1 );
	int fupedge = (int) fithist->GetBinCenter( vsize );
	int lastbin = *it_max;
	
	for( int i=0; it_max+i < yvalues.end(); ++i )
	{
/*		if( it_max[i] < 5 )
		{
//			std::cout << "shifting fit range to bin " <<  it_max - yvalues.begin() + i + 1 << std::endl;
			fupedge = (int) hist->GetBinCenter( it_max - yvalues.begin() + i );
			break;
		}*/
		if( it_max[i] >= lastbin && it_max[i] < 40 )
		{
			//std::cout << "try to exclude undershoot rise -> bin " << it_max - yvalues.begin() + i << std::endl;
			fupedge = (int) FitFunc::round( fithist->GetBinCenter( it_max - yvalues.begin() + i - 1 ) );
			break;
		}
		lastbin = it_max[i];
	}

	double *parameter = FitFunc::set_parameter_us( crate_number );
	func = new TF1( "fit function", this, &FitFunc::Fit_LLu, floedge, fupedge, 6, "FitFunc", "LLu" );
	func->SetLineColor( 4 );

	func->SetParameter( 0, tvalues[tmax_pos] );
	func->SetParameter( 1, (*it_max-parameter[4]) * exp( 0.5 ));
	func->SetParameter( 2, parameter[2] );
	func->SetParLimits( 2, 1., 30.); 
	func->SetParLimits( 3, 1., 50. ); 
	func->SetParameter( 3, parameter[3] );
	func->FixParameter( 4, parameter[4] );
	func->SetParameter( 5, parameter[5] );

	//m_canvas = new TCanvas( "cff11", "cff11", 800, 600 );
	fithist->Fit( "fit function", "QR" );
	m_tmax = (int) func->GetParameter( 0 );
	m_amp = (int) ( func->GetParameter( 1 ) *exp( -0.5 ) + parameter[4] );
	double* para= func->GetParameters();
	
	int goflong = FitFunc::do_gof_llu( yvalues, tvalues.at( it_max - yvalues.begin() ), para);
	gof = do_simple_gof( goflong );
	
	char htitle[4];
	sprintf( htitle, "%d", goflong );
	fithist->SetTitle( htitle );
	
	fithist->GetYaxis()->SetRangeUser( 0, *it_max*1.2 );

	FitFunc::set_histo_color( fithist, gof );
	//m_canvas->Clear();

	tmax = m_tmax;
	amp = m_amp;
	m_para = para;

	return;
}

void FitFunc::do_slice_fit_lluF(std::vector<int> yvalues, int crate_number, int &tmax, int &amp, double &gof, std::string hname ) // hname = "hff"
{
	int vsize = yvalues.size();

	std::vector<int> tvalues = fill_tvector( vsize );
	fithist = fill_histo( tvalues, yvalues, hname ); 

	//find fit region
	std::vector<int>::iterator it;
	std::vector<int>::iterator it_max;
	it_max = std::max_element( yvalues.begin(), yvalues.end() );
	int tmax_pos = it_max - yvalues.begin();
	
	// check if fit region is valid, else simply return the hist and set gof=-2
	if( it_max < yvalues.begin()+2 || it_max > yvalues.end()-3 )
	{
		tmax = tvalues[tmax_pos];
		amp = *it_max;
		gof = -2;
		return;
	}

	
	int floedge = (int) fithist->GetBinCenter( 1 );
	int fupedge = (int) fithist->GetBinCenter( vsize );
	int lastbin = *it_max;
	
	for( int i=0; it_max+i < yvalues.end(); ++i )
	{
/*		if( it_max[i] < 5 )
		{
//			std::cout << "shifting fit range to bin " <<  it_max - yvalues.begin() + i + 1 << std::endl;
			fupedge = (int) hist->GetBinCenter( it_max - yvalues.begin() + i );
			break;
		}*/
		if( it_max[i] >= lastbin && it_max[i] < 40 )
		{
			//std::cout << "try to exclude undershoot rise -> bin " << it_max - yvalues.begin() + i << std::endl;
			fupedge = (int) FitFunc::round( fithist->GetBinCenter( it_max - yvalues.begin() + i - 1 ) );
			break;
		}
		lastbin = it_max[i];
	}

	double *parameter = FitFunc::set_parameter_usF( crate_number );
	func = new TF1( "fit function", this, &FitFunc::Fit_LLuF, floedge, fupedge, 6, "FitFunc", "LLuF" );
	func->SetLineColor( 4 );

	func->SetParameter( 0, tvalues[tmax_pos] );
	func->SetParameter( 1, (*it_max-parameter[4]) * exp( 0.5 ));
	func->SetParameter( 2, parameter[2] );
	func->SetParLimits( 2, 1., 30. ); 
	func->SetParLimits( 3, 1., 50. ); 
	func->SetParameter( 3, parameter[3] );
	func->FixParameter( 4, parameter[4] );
	func->SetParameter( 5, parameter[5] );

	//m_canvas = new TCanvas( "cff11", "cff11", 800, 600 );
	fithist->Fit( "fit function", "QR" );
	m_tmax = (int) func->GetParameter( 0 );
	m_amp = (int) ( func->GetParameter( 1 ) *exp( -0.5 ) + parameter[4] );
	double* para= func->GetParameters();
	
	int goflong = FitFunc::do_gof_llu( yvalues, tvalues.at( it_max - yvalues.begin() ), para);
	gof = do_simple_gof( goflong );
	
	char htitle[4];
	sprintf( htitle, "%d", goflong );
	fithist->SetTitle( htitle );
	
	fithist->GetYaxis()->SetRangeUser( 0, *it_max*1.2 );

	FitFunc::set_histo_color( fithist, gof );
	//m_canvas->Clear();

	tmax = m_tmax;
	amp = m_amp;
	m_para = para;

	return;
}

double* FitFunc::return_parameters(){
	return m_para;
}

TH1D* FitFunc::return_histogram(const char* histname)
{
	fithist->SetName(histname);
	return fithist;
}



TF1* FitFunc::setup_fit(CaloPartEnum part, double* par, int floedge, int fupedge)
{
	switch( part ){
		case LArFCALEM:
			par[2] = 13;
			par[3] = 16;
			par[4] = 32;
			par[5] = 100.;

			func = new TF1( "fit function", this, &FitFunc::Fit_LLu, floedge, fupedge, 6, "FitFunc", "LLu" );
			func->SetLineColor( 4 );

			func->SetParameter( 2, par[2] );
			func->SetParameter( 3, par[3] );
			func->SetParLimits( 2, 10., 40. ); 
			func->SetParLimits( 3, 10., 50. ); 
			func->FixParameter( 4, par[4] );
			func->SetParameter( 5, par[5] );
			//func->SetParLimits( 5, 100., 300. );
			return func;

		case LArFCALHAD2:
			par[2] = 23;
			par[3] = 14;
			par[4] = 32;
			par[5] = 100.;

			func = new TF1( "fit function", this, &FitFunc::Fit_LLu, floedge, fupedge, 6, "FitFunc", "LLu" );
			func->SetLineColor( 4 );

			func->SetParameter( 2, par[2] );
			func->SetParameter( 3, par[3] );
			func->SetParLimits( 2, 10., 40.  ); 
			func->SetParLimits( 3, 10., 50.  ); 
			func->FixParameter( 4, par[4] );
			func->SetParameter( 5, par[5] );
			//func->SetParLimits( 5, 100., 300. );
			return func;

		case LArFCALHAD3:
			par[2] = 18;
			par[3] = 18;
			par[4] = 32;
			par[5] = 100.;

			func = new TF1( "fit function", this, &FitFunc::Fit_LLu, floedge, fupedge, 6, "FitFunc", "LLu" );
			func->SetLineColor( 4 );

			func->SetParameter( 2, par[2] );
			func->SetParameter( 3, par[3] );
			func->SetParLimits( 2, 10., 40.  ); 
			func->SetParLimits( 3, 10., 50.  ); 
			func->FixParameter( 4, par[4] );
			func->SetParameter( 5, par[5] );
			//func->SetParLimits( 5, 100., 300. );
			return func;

		case LArEMEC:
			par[2] = 21;
			par[3] = 29;
			par[4] = 32;
			par[5] = 100;

			func = new TF1( "fit function", this, &FitFunc::Fit_LLu, floedge, fupedge, 6, "FitFunc", "LLu" );
			func->SetLineColor( 4 );

			func->SetParameter( 2, par[2] );
			func->SetParameter( 3, par[3] );
			func->SetParLimits( 2, 10., 40. ); 
			func->SetParLimits( 3, 10., 50. ); 
			func->FixParameter( 4, par[4] );
			func->SetParameter( 5, par[5] );
			return func;

		case LArOverlap:
			par[2] = 16;
			par[3] = 15;
			par[4] = 32;
			par[5] = 100.;

			func = new TF1( "fit function", this, &FitFunc::Fit_GLu, floedge, fupedge, 6, "FitFunc", "GLu" );
			func->SetLineColor( 4 );

			func->SetParameter( 2, par[2] );
			func->SetParameter( 3, par[3] );
			func->SetParLimits( 2, 10., 40. ); 
			func->SetParLimits( 3, 10., 50. ); 
			func->FixParameter( 4, par[4] );
			func->SetParameter( 5, par[5] );
			return func;

		case LArEMB:
			par[2] = 18;
			par[3] = 29;
			par[4] = 30;
			par[5] = 100.;

			func = new TF1( "fit function", this, &FitFunc::Fit_GLu, floedge, fupedge, 6, "FitFunc", "GLu" );
			func->SetLineColor( 4 );

			func->SetParameter( 2, par[2] );
			func->SetParameter( 3, par[3] );
			func->SetParLimits( 2, 10., 40. ); 
			func->SetParLimits( 3, 10., 50. ); 
			func->FixParameter( 4, par[4] );
			func->SetParameter( 5, par[5] );
			return func;

		case LArHEC:
			par[2] = 18;
			par[3] = 18;
			par[4] = 32;
			par[5] = 100.;

			func = new TF1( "fit function", this, &FitFunc::Fit_LLu, floedge, fupedge, 6, "FitFunc", "LLu" );
			func->SetLineColor( 4 );

			func->SetParameter( 2, par[2] );
			func->SetParameter( 3, par[3] );
			func->SetParLimits( 2, 10., 40.  ); 
			func->SetParLimits( 3, 10., 50.  ); 
			func->FixParameter( 4, par[4] );
			func->SetParameter( 5, par[5] );
			return func;

		case TileEB:
			par[2] = 23;
			par[3] = 18;
			par[4] = 32;
			par[5] = 100.;

			func = new TF1( "fit function", this, &FitFunc::Fit_GLu, floedge, fupedge, 6, "FitFunc", "GLu" );
			func->SetLineColor( 4 );

			func->SetParameter( 2, par[2] );
			func->SetParameter( 3, par[3] );
			func->SetParLimits( 2, 10., 40.  ); 
			func->SetParLimits( 3, 10., 50.  ); 
			func->FixParameter( 4, par[4] );
			func->SetParameter( 5, par[5] );
			return func;

		case TileLB:
			par[2] = 22;
			par[3] = 15;
			par[4] = 32;
			par[5] = 100.;

			func = new TF1( "fit function", this, &FitFunc::Fit_GLu, floedge, fupedge, 6, "FitFunc", "GLu" );
			func->SetLineColor( 4 );

			func->SetParameter( 2, par[2] );
			func->SetParameter( 3, par[3] );
			func->SetParLimits( 2, 10., 40. ); 
			func->SetParLimits( 3, 10., 50. ); 
			func->FixParameter( 4, par[4] );
			func->SetParameter( 5, par[5] );
			return func;

		case undef:
			return NULL;

	}

}



int FitFunc::get_gof(std::vector<int> yvalues, int tpeak, double* parameter, CaloPartEnum part)
{

	if ((part == LArEMEC) || (part == LArFCALEM) || (part == LArFCALHAD2) || (part == LArFCALHAD3) || (part == LArHEC) ){
		int goflong = FitFunc::do_gof_llu( yvalues, tpeak, parameter);
		return goflong;
	}
	else{
		int goflong = FitFunc::do_gof_glu( yvalues, tpeak, parameter);
		return goflong;
	}
}

double FitFunc::round( double in, int prec )
{
	double in1 = in * pow( 10, prec );
	return floor( in1 + 0.5 ) / pow( 10, prec );
}

CaloPartEnum FitFunc::GetDetectorPart(double etaIn, double layer){

    CaloPartEnum part =  undef;

    if(layer == 0)
    {
        if      (etaIn < -3.2 ) { part = LArFCALEM; }
        else if (etaIn < -1.5 ) { part = LArEMEC; }
        else if (etaIn < -1.4 ) { part = LArOverlap; }
        else if (etaIn <   0. ) { part = LArEMB; }
        else if (etaIn <  1.4 ) { part = LArEMB; }
        else if (etaIn <  1.5 ) { part = LArOverlap; }
        else if (etaIn <  3.2 ) { part = LArEMEC; }
        else if (etaIn >=  3.2 ){ part = LArFCALEM; }
    }
    else if (layer==1)
    {
           if      (etaIn < -4.6 ) { part = LArFCALHAD2; }
           else if (etaIn < -4.2 ) { part = LArFCALHAD3; }
           else if (etaIn < -3.8 ) { part = LArFCALHAD2; }
           else if (etaIn < -3.2 ) { part = LArFCALHAD3; }
           else if (etaIn < -1.5 ) { part = LArHEC; }
           else if (etaIn < -0.9 ) { part = TileEB; }
           else if (etaIn <   0. ) { part = TileLB; }
           else if (etaIn <  0.9 ) { part = TileLB; }
           else if (etaIn <  1.5 ) { part = TileEB; }
           else if (etaIn <  3.2 ) { part = LArHEC; }
           else if (etaIn <  3.8 ) { part = LArFCALHAD2; }
           else if (etaIn <  4.2 ) { part = LArFCALHAD3; }
           else if (etaIn <  4.6 ) { part = LArFCALHAD2; }
           else if (etaIn >=  4.6 ){ part = LArFCALHAD3; }
    }
    return part;
}
