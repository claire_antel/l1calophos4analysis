#ifndef PHOS4ANALYSIS_RECONSTRUCTP4_H
#define PHOS4ANALYSIS_RECONSTRUCTP4_H 1

#include "AthenaBaseComps/AthHistogramAlgorithm.h"
#include <iostream>
#include <fstream>
#include "FitFunc.h"

class reconstructp4: public ::AthHistogramAlgorithm { 
 public: 
  reconstructp4( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~reconstructp4(); 

  virtual StatusCode  initialize();
  virtual StatusCode  execute();
  virtual StatusCode  finalize();

 private: 

  double step_to_ns = 25./24.; 
  int max_stepno = 25;
  int m_block_stepno;
  //int m_block_nrods;
  int m_skip=0;
  unsigned int m_run;
  std::vector<unsigned int> m_ids;

  ofstream m_output;

  CaloPartEnum GetDetectorPart(double etaIn, double layer);
  std::pair<double, double> computeMandERR(std::vector<unsigned int> vec);
  //std::map<unsigned int, unsigned int> m_idlist;

  ofstream m_pedestal_info;
  
//   std::map< unsigned int, std::map< unsigned int, std::vector< unsigned int > > > temp_map;


  std::map<unsigned int, std::map<unsigned int, std::vector<unsigned int>> > m_fulladcs;
/*  std::map<unsigned int, std::vector<double> > m_sumsq;
  std::map<unsigned int, std::vector<double> > m_sum;*/
  std::map<unsigned int, std::vector<double> > m_ADCmean;
  std::map<unsigned int, std::vector<double> > m_ADCerr;
  std::map<unsigned int, double> m_eta;
  std::map<unsigned int, double> m_phi;
  std::map<unsigned int, double> m_layer;
  std::map<unsigned int, double> m_part;
  std::map<unsigned int, double> m_sat;
  std::map<unsigned int, double> m_nosignal;
  std::map<unsigned int, double> m_pedmean;
  std::map<unsigned int, double> m_pederr;
  std::map<unsigned int, double> m_P4step;
  std::map<unsigned int, double> m_ped;
  std::map<unsigned int, std::vector<int> > m_ped_vec;
  std::vector<std::map<unsigned int, double > > v_pedmean;
  std::vector<std::map<unsigned int, double > > v_pederr;
  //std::map<unsigned int, TH1D*> m_reconpulses;
  std::vector<std::map<unsigned int, std::vector<double> > > v_ADCmean_maps;
  std::vector<std::map<unsigned int, std::vector<double> > > v_ADCerr_maps;

  void write_Histmap(unsigned int id);

  

}; 

#endif //> !PHOS4ANALYSIS_RECONSTRUCTP4_H
