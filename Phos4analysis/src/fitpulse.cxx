#include <iostream>
#include <iomanip>
#include <vector>
#include <map>
#include <sstream>
#include <string>
#include <fstream>
#include <algorithm> 
#include "TTxml.h"
#include "FitFunc.h"
		
#ifndef __CINT__
#include "TString.h"
#include "TF1.h"
#include "TFile.h"
#include "TKey.h"
#include "TObject.h"
#include "TProfile.h"
#include "TH1.h"
#include "TH1D.h"
#endif

void fitpulse(const char* inputfile="smoothP4pulses.root",const char* outputfile="fittedP4pulses.root"){

      std::cout << "IN! "<< std::endl;

      FitFunc fit;
      TH1D* hist;      

      char inputpath_and_file[64];
      sprintf(inputpath_and_file, "$HOME/20.1.3.3./Phos4analysis/output/%s", inputfile);
      TFile file1(inputfile);
      char outputpath_and_file[64];
      sprintf(outputpath_and_file, "$HOME/20.1.3.3./Phos4analysis/output/%s", outputfile);
      TFile file2(outputpath_and_file,"RECREATE");
      std::map<unsigned int, double> eta_list;
      std::map<unsigned int, double> layer_list;

      std::cout << "file size... " <<file1.GetSize()<< std::endl;
	

 //     TFile* subdir=gDirectory->GetFile();
   //   subdir->GetListOfKeys()->Print();

      TTxml xml_ids;
      xml_ids.Read("/afs/cern.ch/user/c/cantel/20.1.3.3./run/TTproperties.xml");
      eta_list = xml_ids.Get("etas");
      layer_list = xml_ids.Get("layers");

      int cnt=0;

      for (std::map<unsigned int, double>::iterator i =eta_list.begin(); i!=eta_list.end(); i++){
		if (cnt>=5) continue;
		cnt++;
      		file1.cd(); 
		unsigned int id = i->first;
		double eta = eta_list[id];
		double layer = layer_list[id]; 
	      	char histname[30];
	      	sprintf(histname, "reconpulse_id%u", id);
		gDirectory->Print();
	      	gDirectory->GetObject(histname,hist);
		std::cout << "histname is... " << histname << std::endl;	
		std::cout << "hist is... " << hist << std::endl;
	       
	      	if (hist != 0){
			file2.cd();
			std::cout << "hist exists. "<< std::endl;
			// do fitting
			
			int amp= hist->GetMaximum();
			int binmax = hist->GetMaximumBin();
   			int tpeak = hist->GetXaxis()->GetBinCenter(binmax);
			double gof=0;
			std::cout << "amp is... " << amp << std::endl;	
			std::cout << "tpeak is... " << tpeak << std::endl;
			CaloPartEnum part = fit.GetDetectorPart(eta, layer);
			std::cout << "part... " << part << std::endl;		

			char newHistName[40];
			sprintf(newHistName,"fittedpulse_part%uid%u",part, id);

			std::vector<int> y_vec = fit.read_histo(hist);
			std::vector<int> t_vec = fit.fill_tvector(y_vec.size());
			std::cout << "y_vec max... " << *std::max_element(y_vec.begin(), y_vec.end()) << std::endl;	
		        bool fitted = fit.do_slice_fit_us(y_vec, part, tpeak, amp, gof);
 		        if (not fitted) continue;
			double* para=fit.return_parameters();
			std::cout << "2nd parameter.. " << para[2] << std::endl;
			TH1D* newHist;
			newHist=fit.return_histogram(newHistName);			
			//TH1D* newHist=fit.fill_histo(t_vec, y_vec, newHistName);
			newHist->GetXaxis()->SetTitle("Time [ns]");
			newHist->GetYaxis()->SetTitle("Height [ADC counts]");
			std::cout << "new hist title.. " << newHist->GetTitle() << std::endl;

			newHist->Write();

		}
     }
     if( hist != 0 )
     {
		delete hist;
		hist = 0;
     }
    /* if(newHist != 0 )
     {
		delete newHist;
		newHist = 0;
     }*/
     std::cout << "closing down... " << std::endl;	
     file1.Close();
     file2.Close();
}
