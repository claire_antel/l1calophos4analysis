#include <TH1.h>
#include <TF1.h>
#include "TMath.h"
#include <sstream>
#include "TFile.h"
#include "FitFunctions.h"

#include <iostream>
using namespace std;

//Fitfunction for a parabola
double Fit_Pa(double *x, double *par){
	double xmax = par[0];
	double ymax = par[1];
	double width = par[2];
        double pedestal = par[4];
	
	return (-width*(x[0] - xmax)*(x[0] - xmax)+ymax+pedestal);
}

//Fitfunction for a Parabola-Parabola
double Fit_PP(double *x, double *par){
	double xmax = par[0];
	double ymax = par[1];
        double sigma_l = par[2];
        double sigma_r = par[3];
        double pedestal = par[4];
	
	if ( x[0] <= par[0]){
		return (-sigma_l*(x[0] - xmax)*(x[0] - xmax)+ymax+pedestal);
	}
	if (x[0] > par[0]){
		return (-sigma_r*(x[0] - xmax)*(x[0] - xmax)+ymax+pedestal);
	}

	else return -1;

}

//FitFunction for a Gauss
double Fit_G(double *x, double *par) {
      double xmax = par[0];
      double ampl = par[1];
      double sigma_g = par[2];
      double pedestal = par[4];
     
      return( ampl*TMath::Exp(-0.5*(x[0]-xmax)*(x[0]-xmax)/(sigma_g*sigma_g)-0.5)+pedestal);
}

//Fitfunction for a Gauss-Gauss
double Fit_GG(double *x, double *par) {
      double xmax = par[0];
      double ampl = par[1];
      double sigma_g1 = par[2];
      double sigma_g2 = par[3];
      double pedestal = par[4];
     
      if ( x[0] <= par[0]){
        //Gauss function
        return( ampl*TMath::Exp(-0.5*(x[0]-xmax)*(x[0]-xmax)/(sigma_g1*sigma_g1)-0.5)+pedestal);
      }
      if (x[0] > par[0]){
        //Gauss function
        return( ampl*TMath::Exp(-0.5*(x[0]-xmax)*(x[0]-xmax)/(sigma_g2*sigma_g2)-0.5)+pedestal);
      }
   
      else return -1;
}

//Fitfunction for a Gauss-Landau -> reference fit for all CaloDivisions except HEC
double Fit_GL(double *x, double *par) {
      double xmax = par[0];
      double ampl = par[1];
      double sigma_g = par[2];
      double sigma_l = par[3];
                double pedestal = par[4];
     
      if ( x[0] <= par[0]){
        //Gauss function
        return( ampl*TMath::Exp(-0.5*(x[0]-xmax)*(x[0]-xmax)/(sigma_g*sigma_g)-0.5)+pedestal);
      }
      if (x[0] > par[0]){
        //Landau function
        return( ampl*TMath::Exp(-0.5*((x[0]-xmax)/sigma_l + TMath::Exp(-(x[0]-xmax)/sigma_l)))+pedestal);
      }
   
      else return -1;
}

//Fitfunction for a Gauss-Landau, considering the undershoot on the Landau side
double Fit_GLu(double *x, double *par) {
      double xmax = par[0];
      double ampl = par[1];
      double sigma_g = par[2];
      double sigma_l = par[3];
      double undershoot = par[4];
                double pedestal = par[5];
     
      if ( x[0] <= par[0]){
        //Gauss function
        return( ampl*TMath::Exp(-0.5*(x[0]-xmax)*(x[0]-xmax)/(sigma_g*sigma_g)-0.5)+pedestal);
      }
      if (x[0] > par[0]){
        //Landau function
        return( (ampl+undershoot*TMath::Exp(0.5))*TMath::Exp(-0.5*((x[0]-xmax)/sigma_l + TMath::Exp(-(x[0]-xmax)/sigma_l)))+pedestal-undershoot);
      }
   
      else return -1;
}


//Fitfunction for a double Landau -> reference fit for HEC
double Fit_LL(double *x, double *par) {
      double xmax = par[0];
      double ampl = par[1];
      double sigma_l1 = par[2];
      double sigma_l2 = par[3];
                double pedestal = par[4];
     
      if ( x[0] <= par[0]){
        //Landau function 1
        return(ampl*TMath::Exp(-0.5*((x[0]-xmax)/sigma_l1 + TMath::Exp(-(x[0]-xmax)/sigma_l1))) +pedestal);
      }
      if (x[0] > par[0]){
        //Landau function 2
        return(ampl*TMath::Exp(-0.5*((x[0]-xmax)/sigma_l2 + TMath::Exp(-(x[0]-xmax)/sigma_l2))) +pedestal);
      }
   
      else return -1;
}


//Fitfunction for a double Landau (with undershoot on right side)
double Fit_LLu(double *x, double *par) {
      double xmax = par[0];
      double ampl = par[1];
      double sigma_l1 = par[2];
      double sigma_l2 = par[3];
      double undershoot = par[4];
                double pedestal = par[5];
     
      if ( x[0] <= par[0]){
        //Landau function
        return(ampl*TMath::Exp(-0.5*((x[0]-xmax)/sigma_l1 + TMath::Exp(-(x[0]-xmax)/sigma_l1))) +pedestal);
      }
      if (x[0] > par[0]){
        //Landau function with undershoot
        return( (ampl+undershoot*TMath::Exp(0.5))*TMath::Exp(-0.5*((x[0]-xmax)/sigma_l2 + TMath::Exp(-(x[0]-xmax)/sigma_l2)))+pedestal-undershoot);
      }
   
      else return -1;
}
