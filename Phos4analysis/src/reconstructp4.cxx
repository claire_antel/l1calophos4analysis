// Phos4analysis includes
#include "TH2TT.h"
#include "reconstructp4.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTrigL1Calo/RODHeaderContainer.h"
#include "xAODTrigL1Calo/RODHeaderAuxContainer.h"
#include "xAODTrigL1Calo/TriggerTowerContainer.h"
#include "xAODTrigL1Calo/TriggerTowerAuxContainer.h"
#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/ServiceHandle.h"
#include <TH1D.h>
#include <TMultiGraph.h>
#include <TGraphErrors.h>
#include "TTxml.h"

#include <algorithm>
#include <vector>
#include <cmath>
#include <stdio.h>
#include <float.h>

reconstructp4::reconstructp4( const std::string& name, ISvcLocator* pSvcLocator ) : AthHistogramAlgorithm( name, pSvcLocator ){

  declareProperty( "run", m_run );

}


reconstructp4::~reconstructp4() {}


StatusCode reconstructp4::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");

  m_block_stepno=0;
  //m_block_nrods=0;

  m_output.open("outputfile_recon.txt");
  //m_output << "ROD step number \n";

  m_pedestal_info.open("ped_info.txt");
  m_pedestal_info << "sum \t vec.size \t P4 step \t  mean \t error \n";

  return StatusCode::SUCCESS;
}

StatusCode reconstructp4::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  
//   CHECK(book(TH2D("tempHist", "adc rms values by rod step and slice; slice; rod step", 15, -0.5, 14.5, 25, -0.5, 24.5)));  
//   hist("tempHist")->SetOption("colz");
//   //std::cout<<"about to fill log. "<<std::endl;
//   std::map< unsigned int, std::map< unsigned int, std::vector< unsigned int > > >::iterator it;
//   for (it= temp_map.begin(); it != temp_map.end(); it++) {
//       //std::cout<<"outputting to log.."<<std::endl;
//       unsigned int blockno = it->first;
//       m_output<<"\n*** At p4 step "<<blockno<<" ***\n";
//       std::map<unsigned int, std::vector<unsigned int>> temp_map2 = it->second;
//       std::map< unsigned int, std::vector< unsigned int >>::iterator it2;
//       for (it2= temp_map2.begin(); it2 != temp_map2.end(); it2++) {
// 	unsigned int slice = it2->first;
// 	m_output<<"\n*** At slice "<<slice<<" ***\n";
// 	std::vector< unsigned int > temp_vec = it2->second;
// 	for (std::vector<unsigned int>::const_iterator it3 = temp_vec.begin(); it3 !=temp_vec.end(); it3++)
// 	    m_output<<*it3<<' ';
// 	std::pair<double, double> temp_pair = computeMandERR(temp_vec);
// 	m_output<<"\n---- standard error: "<<temp_pair.second<<"-----\n";
// 	unsigned int rodbin = hist("tempHist")->GetYaxis()->FindBin(blockno);
// 	unsigned int slicebin = hist("tempHist")->GetXaxis()->FindBin(slice);	
// 	hist("tempHist")->SetBinContent(slicebin, rodbin,temp_pair.second); 
//       }
//   }

  
  
  TH2TT *pedvar_EM_hist = new TH2TT("pedvar_EM_hist", "pedestal variation in EM;#eta;#phi");
  TH2TT *pedvar_HAD_hist = new TH2TT("pedvar_HAD_hist", "pedestal variation in HAD;#eta;#phi");
  pedvar_EM_hist->SetOption("colz");
  pedvar_HAD_hist->SetOption("colz");
  TMultiGraph *multigraph = new TMultiGraph("pedvals_mg", "ped values as fn of PHOS4 step");
  TFile *ped_graphs = new TFile();
  ped_graphs->Open("ped_graphs.root", "Recreate");
  int cntg =0;

  if ((m_ids.size()!=m_eta.size()) || (m_ids.size()!=m_phi.size()) || (m_ids.size()!=m_layer.size())){
	ATH_MSG_DEBUG ("maps not of same size. raising error.");
	return StatusCode::FAILURE;
  }

  int tempcnt=0;

  for (std::vector<unsigned int>:: iterator id = m_ids.begin(); id != m_ids.end(); ++id) {

	  unsigned int satcnt=0;
  	  double maxadc=0.;
	  double sum_ped = 0.;

	  int part = (int) m_part[*id];

	  if (v_pedmean.size()!=v_ADCmean_maps.size()){
		ATH_MSG_DEBUG ("vectors not of same size. raising error.");
		return StatusCode::FAILURE;
	  }

	  char hname[64];
	  sprintf(hname, "reconpulse_part%d_id%08x", part, *id);
	  //TH1D* hist_reconpulse= new TH1D(hname, "adc curve", 375, 0, 375);
	  CHECK(book(TH1D(hname, "adc curve", 360, 0, 374.4)));
//	  char hname_ped[64];
	  TGraphErrors *graph = new TGraphErrors(v_pedmean.size());
//	  sprintf(hname_ped, "pedvariation_id%u", *id);
	  //TH1D* hist_reconpulse= new TH1D(hname, "adc curve", 375, 0, 375);
//	  CHECK(book(TH1D(hname_ped, "mean ped values", 25, -0.5, 24.5)));

	  int etabin = pedvar_EM_hist->GetXaxis()->FindBin(m_eta[*id]);
	  int phibin = pedvar_EM_hist->GetYaxis()->FindBin(m_phi[*id]);
	  int layer = (int) m_layer[*id];

	  double maxped = 0.;
	  double minped = 1000.; 

	  //m_output<<"----------- NEW TOWER ------------ \n"; 

	  //m_output<<"size of adc and ped vecs... "<<v_ADCmean_maps.size()<<" and "<<v_pedmean.size();
	  for ( unsigned int step = 0; step < v_ADCmean_maps.size(); step++ ){
		//m_output<<"----------- AT STEP "<<step<<" ------------ \n"; 
		double meanped=(v_pedmean.at(step))[*id];
		sum_ped += meanped;
		double pedcorrection = 32.-meanped;
		double errped=(v_pederr.at(step))[*id];
		std::vector<double> meanvec=(v_ADCmean_maps.at(step))[*id];
		std::vector<double> errvec=(v_ADCerr_maps.at(step))[*id];
		//m_output<<"vector size... "<<meanvec.size()<<"\n";
		for (unsigned int n =0; n<meanvec.size(); n++){
			double time = (n*24+step)*step_to_ns;
			if (tempcnt%5000==0) ATH_MSG_DEBUG("time step: "<<time);
			if (meanvec.at(n)>=1023) satcnt++;
			if (meanvec.at(n)>=maxadc) maxadc = meanvec.at(n);
			//m_output<<"adc count and bin error : "<<meanvec.at(n)<<", "<<errvec.at(n)<<"\n";
			//m_output<<"time... "<<time<<" adc cnt... "<<meanvec.at(n)<<" adc cnt error... "<<errvec.at(n)<<"\n";
			unsigned int timebin = hist(hname)->GetXaxis()->FindBin(time);
			hist(hname)->SetBinContent(timebin, meanvec.at(n)+pedcorrection); 
			hist(hname)->SetBinError(timebin, errvec.at(n)); // errors on the order of 0.03 ADC (for run I was on the order 0.3) ...will need to adjust chi2/ndf limit?				
		}
		graph->SetPoint(step, step, meanped);
		graph->SetPointError(step, 0, errped);
		if (meanped>maxped) maxped=meanped;
		if (meanped<minped) minped=meanped;
		//step++;
	 }
	
	 tempcnt++;
	 double avg_ped = sum_ped/(v_ADCmean_maps.size());
         m_ped[*id] = avg_ped;		

	 if (satcnt>0) m_sat[*id]=1.;
	 else m_sat[*id]=0.;
	 if (maxadc<=70.) {
		//m_output<<"max too low for ID "<<*id<<"\n";
		m_nosignal[*id] = 1.;
		continue;
	 }
	 else m_nosignal[*id] = 0.;

         //m_output<<"next id computed is "<< *id<<"with adc max "<<maxadc<<"\n";

	 double pedvar = maxped-minped;
	 //if (pedvar>=3.) m_output<<"TT caught red-handed! id"<<*id<<"with amp "<<pedvar<<"\n";
	 if (layer==0) pedvar_EM_hist->SetBinContent(etabin, phibin, pedvar);
	 else pedvar_HAD_hist->SetBinContent(etabin, phibin, pedvar);

	 //graph->SetLineWidth(0);
	 graph->SetMarkerStyle(7);
	 graph->SetMarkerColor(37);
      	 char gname[64];
  	 sprintf(gname, "pedvalues id%08x (osc. amp = %f)", *id, pedvar);
	 graph->SetTitle(gname);

	 //graph->Draw("p");
	 if (cntg % 500 == 0 || *id==18089216 || *id == 101976833 || *id == 118818304 || *id == 119341568) {
		graph->Write();
		multigraph->Add(graph, "lp");
	 }

	 cntg++;
	  //m_reconpulses[*id]=hist_reconpulse;
	
  }

  multigraph->Write();
  ped_graphs->Close();
  pedvar_EM_hist->Write();
  pedvar_HAD_hist->Write();

  m_pedestal_info.close();
  m_output.close();

  TTxml TTproperties_list;
  TTproperties_list.Add("etas", m_eta);
  TTproperties_list.Add("phis", m_phi);
  TTproperties_list.Add("layers", m_layer);
  TTproperties_list.Add("parts", m_part);
  TTproperties_list.Add("pedestal", m_ped);
  TTproperties_list.Add("saturated", m_sat);
  TTproperties_list.Add("nosignal", m_nosignal);

  char xmlname[90];
  sprintf(xmlname, "/afs/cern.ch/work/c/cantel/private/output/%d/TTproperties_run%d.xml", m_run, m_run);
  TTproperties_list.Write(xmlname);


  return StatusCode::SUCCESS;
}

StatusCode reconstructp4::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  
  const xAOD::EventInfo* eventInfo = 0;

  CHECK( evtStore()->retrieve( eventInfo) );


  //ATH_MSG_DEBUG(evtStore ()-> dump());

  //---------
  // accessing rod header
  //---------
  const xAOD::RODHeaderContainer* RODs = 0;
 
 // StatusCode sc =  
  CHECK(evtStore()->retrieve( RODs, "RODHeaders")) ;

  //---------
  // accessing trigger towers
  //---------

  const xAOD::TriggerTowerContainer* TTs = 0;
 
  CHECK(evtStore()->retrieve( TTs, "xAODTriggerTowers")) ;

  // create empty maps ... 

  if (m_skip==0){
	  unsigned int ttcnt=0;
	  for ( const auto* TT : *TTs ) {
			unsigned int friendid = TT->coolId();
			double layer = TT->layer();
			double eta = TT->eta();
			double phi = TT->phi();
			CaloPartEnum part = GetDetectorPart(eta, layer);
			m_ids.push_back(friendid);
			//m_idlist[ttcnt]=friendid;
			m_eta[friendid]=eta;
			m_phi[friendid]=phi;
			m_part[friendid]=part;
			m_layer[friendid]=layer;
			std::vector<uint_least16_t> adc_vec = TT->adc();
			unsigned int nslices = adc_vec.size();
			std::vector<double> tmp;
			for ( unsigned int slice = 0; slice != nslices; slice++ ){
				tmp.push_back(0.);
			} 
/*			m_sum[friendid] = tmp;
			m_sumsq[friendid] = tmp;*/
			m_ADCmean[friendid] = tmp;
			m_ADCerr[friendid] = tmp;
			ttcnt++;
	  }
   m_skip++;
   }
   
  bool m_changemaxpos(1);
  unsigned int fixedmax(0);

  for ( const auto* ROD : *RODs ) {

	int rod_stepno = (ROD->stepNumber());
	if (rod_stepno>24) continue; //int stepno = rod_stepno % max_stepno
	//m_output<<"rod step number is... "<<rod_stepno<<"\n";

	if (rod_stepno==m_block_stepno){
		  //m_output<<"*** still accumulating adc cnts at current step ***"<<"\n";
		  //m_block_nrods++;
		  
		  for ( const auto* TT : *TTs ) {
		      	  auto coolId = TT->coolId();
			  //if (coolId!=101843713) continue;
      			  std::vector<uint_least16_t> adc_vec = TT->adc();
      			  unsigned int nslices = adc_vec.size();
			  //m_output<<"printing out adcs...\n";
			  //for (std::vector<uint_least16_t>::iterator itvec = std::begin(adc_vec); itvec!=std::end(adc_vec); itvec++) m_output<<*itvec<<" ";
			  //m_output<<"\n";
			  uint_least16_t adcpeak = *max_element(std::begin(adc_vec),std::end(adc_vec));
			  //m_output<<"max adc is.. "<<adcpeak<<"\n";
			  if (adcpeak<=50.)continue;
			  //m_output<<"max slice position is.. "<<maxslicepos<<"\n";
			  //if (!(maxslicepos == 7 || maxslicepos == 6)) m_output<<"Tile L1A NOT set right.\n";
/*			  int maxslicepos = std::max_element(std::begin(adc_vec),std::end(adc_vec))- std::begin(adc_vec) ;
			  if (!(maxslicepos == 7 || maxslicepos == 6)) continue;
			  if (m_changemaxpos) {fixedmax = maxslicepos; m_changemaxpos = !m_changemaxpos;}  
			  //m_output<<"Tile L1A set right.\n";
			  if (maxslicepos != fixedmax) continue;*/

			  for ( unsigned int slice = 0; slice != nslices; slice++ ){				
				if (slice<=1){
					m_ped_vec[coolId].push_back(adc_vec[slice]);
				}
				m_fulladcs[coolId][slice].push_back(adc_vec[slice]);
/*			  	m_sum[coolId].at(slice)+=adc_vec[slice];
			  	m_sumsq[coolId].at(slice)+=adc_vec[slice]*adc_vec[slice];*/
				//ATH_MSG_DEBUG("cool ID: "<<coolId);
// 				if (coolId==101712384){
// 					temp_map[m_block_stepno][slice].push_back(adc_vec[slice]);
// 					//m_output<<"*** adc count at slice "<<slice<<" in step "<<m_block_stepno<<" is "<<adc_vec[slice]<<"\n";
// /*					if (m_block_stepno==14){
// 						if (slice==8) m_output<<"*** adc count of slice 8 in step 14... "<<adc_vec[slice]<<"\n";
// 						m_output<<"accumulative sum at this slice... "<<m_sum[coolId].at(slice)<<"\n";
// 					}
// 					if (m_block_stepno==0){
// 						if (slice==8) m_output<<"*** adc count of slice 8 in step 0... "<<adc_vec[slice]<<"\n";
// 						m_output<<"accumulative sum at this slice... "<<m_sum[coolId].at(slice)<<"\n";
// 					}
// 					if (m_block_stepno==14){
// 						if (slice==5) m_output<<"*** adc count of slice 5 in step 14... "<<adc_vec[slice]<<"\n";
// 						m_output<<"accumulative sum at this slice... "<<m_sum[coolId].at(slice)<<"\n";
// 					}*/
// 				}
			  }
		  }
	}
	else if (rod_stepno==m_block_stepno+1){
		m_changemaxpos = !m_changemaxpos;
		ATH_MSG_DEBUG("*** Computing values for step number "<<m_block_stepno<<"*** \n");
		//m_output<<"*** Computing values for step number "<<m_block_stepno<<"*** \n";

// 		std::map<unsigned int,std::vector<double> >::iterator it_mean;
// 		for ( it_mean = m_sum.begin(); it_mean != m_sum.end(); it_mean++ ){
// 			unsigned int nslices=it_mean->second.size();
// 			unsigned int id = it_mean->first;
// 			unsigned int block_nrods = m_norods[id];
// 			if (block_nrods == 0) m_output<<"Total number of rod runs is ZERO for ID"<<id<<"!\n";
// 			if (block_nrods == 0) continue;  
// 			//m_output<<"number of slices within mean vector... "<<nslices<<"\n";
// 			//m_output<<"avg of middle slice... "<<(m_sum[id].at(7))/m_block_nrods<<"\n";
// 			for ( unsigned int slice = 0; slice != nslices; slice++ ){
// 				//mean calculation
// 				m_ADCmean[id].at(slice) = (m_sum[id].at(slice))/block_nrods;//m_block_nrods;
// 				//error calculation of mean
// 				m_ADCerr[id].at(slice) = sqrt((m_sumsq[id].at(slice)-2*m_ADCmean[id].at(slice)*m_sum[id].at(slice)+block_nrods*m_ADCmean[id].at(slice)*m_ADCmean[id].at(slice))/(block_nrods*(block_nrods-1)));
// 			}
		
		std::map<unsigned int, std::map<unsigned int, std::vector<unsigned int>> >::iterator it1;
 		for ( it1 = m_fulladcs.begin(); it1 != m_fulladcs.end(); it1++ ){
		  
		    unsigned int id = it1->first;
		    std::map<unsigned int, std::vector<unsigned int>> fulladcs_byslice = it1->second;
		       		       
		    std::map<unsigned int, std::vector<unsigned int>>::iterator it2;
		    for ( it2 = fulladcs_byslice.begin(); it2 != fulladcs_byslice.end(); it2++ ){	
		      unsigned int slice = it2->first;
		      std::vector<unsigned int> rodvec = it2->second;
		      
		      std::pair<double, double> MandERR;
		      
		      MandERR = computeMandERR(rodvec);
		      double mean = MandERR.first;
		      double err = MandERR.second;
		      
		      ATH_MSG_DEBUG("Error of mean is .. "<<err);
		      		      
		      if (MandERR.second>0.5) {
			std::vector<unsigned int> new_rodvec;
			for (std::vector<unsigned int>::iterator it3 = rodvec.begin(); it3 != rodvec.end(); it3++) {
			    if (fabs(*it3-mean)>20) continue;
			    else new_rodvec.push_back(*it3);
			}
			MandERR = computeMandERR(new_rodvec);
			mean = MandERR.first;
			err =  MandERR.second;
		      }
		      m_ADCmean[id].at(slice) = mean;
		      m_ADCerr[id].at(slice) = err; 
		    }
		    unsigned int n_pedvals = m_ped_vec[id].size();
		    double sum = std::accumulate(std::begin(m_ped_vec[id]), std::end(m_ped_vec[id]), 0.0);

		    double m = (n_pedvals==0) ? 0 : sum / n_pedvals;

		    double accum = 0.0;
		    std::for_each (std::begin(m_ped_vec[id]), std::end(m_ped_vec[id]), [&](const double d) {
		    accum += (d - m) * (d - m);
		    });
		    double stderr = (n_pedvals==0) ? 0 : sqrt(accum /(n_pedvals*n_pedvals));  //(n_pedvals*(n_pedvals-1)));	

		    //m_output<<"*** number of ped values summed...  "<<n_pedvals<<"*** \n";
		    //m_output<<"*** mean and std error...  "<<m<<" / "<<stderr<<"*** \n";

		    m_pedmean[id]=m;
		    m_pederr[id]=stderr;
		    m_P4step[id]=rod_stepno;
		    m_ped_vec[id].clear();

		}
		v_pedmean.push_back(m_pedmean); // vector of maps describing mean of first 3 adc slices per TT, where this vector index is block step.
		v_pederr.push_back(m_pederr); // same, but with error i/o mean.
		v_ADCmean_maps.push_back(m_ADCmean); // vector of maps describing mean-per-slice vector per TT id, where this vector index is block step.
		v_ADCerr_maps.push_back(m_ADCerr);   // same, but with error i/o mean.
	        //m_output<<"block step number and number of rods within step: "<<m_block_stepno<<"/"<<m_block_nrods<<"\n";		
		m_block_stepno++;
		//m_block_nrods=0;

		// reset sum and sumsqrd
 	        for ( const auto* TT : *TTs ) {
	      	  	auto coolId = TT->coolId();
      			std::vector<uint_least16_t> adc_vec = TT->adc();
		  	unsigned int nslices = adc_vec.size();
		  	for ( unsigned int slice = 0; slice != nslices; slice++ ){
/*		  		m_sum[coolId].at(slice)=0;
		  		m_sumsq[coolId].at(slice)=0;*/
				m_fulladcs[coolId][slice].clear();
		  	}
   	        }

	}
  }

  // ==== looking at one test tower ... ======

  //std::vector<double> mean_vals;

  return StatusCode::SUCCESS;
}

CaloPartEnum reconstructp4::GetDetectorPart(double etaIn, double layer){

    CaloPartEnum part =  undef;

    if(layer == 0)
    {
        if      (etaIn < -3.2 ) { part = LArFCALEM; }
        else if (etaIn < -1.5 ) { part = LArEMEC; }
        else if (etaIn < -1.4 ) { part = LArOverlap; }
        else if (etaIn <   0. ) { part = LArEMB; }
        else if (etaIn <  1.4 ) { part = LArEMB; }
        else if (etaIn <  1.5 ) { part = LArOverlap; }
        else if (etaIn <  3.2 ) { part = LArEMEC; }
        else if (etaIn >=  3.2 ){ part = LArFCALEM; }
    }
    else if (layer==1)
    {
           if      (etaIn < -4.6 ) { part = LArFCALHAD2; }
           else if (etaIn < -4.2 ) { part = LArFCALHAD3; }
           else if (etaIn < -3.8 ) { part = LArFCALHAD2; }
           else if (etaIn < -3.2 ) { part = LArFCALHAD3; }
           else if (etaIn < -1.5 ) { part = LArHEC; }
           else if (etaIn < -0.9 ) { part = TileEB; }
           else if (etaIn <   0. ) { part = TileLB; }
           else if (etaIn <  0.9 ) { part = TileLB; }
           else if (etaIn <  1.5 ) { part = TileEB; }
           else if (etaIn <  3.2 ) { part = LArHEC; }
           else if (etaIn <  3.8 ) { part = LArFCALHAD2; }
           else if (etaIn <  4.2 ) { part = LArFCALHAD3; }
           else if (etaIn <  4.6 ) { part = LArFCALHAD2; }
           else if (etaIn >=  4.6 ){ part = LArFCALHAD3; }
    }
    return part;
}

std::pair<double, double> reconstructp4::computeMandERR(std::vector<unsigned int> vec) {
	  // compute rms
	  double sum = std::accumulate(std::begin(vec), std::end(vec), 0.0);
	  double m =  sum / vec.size();
	  double accum = 0.0;
	  std::for_each (std::begin(vec), std::end(vec), [&](const double d) {
	      accum += (d - m) * (d - m);
	  });
	  double stderr = sqrt(accum / (vec.size()*(vec.size()-1)));
	  /*if (isinf(stdev)) {
		std::pair<double, double> mRMS_pair = std::make_pair(-10, -10);
		return mRMS_pair;
		
	  } */
	  if (!std::isnormal(stderr)) stderr = 0;
	  if (!std::isnormal(m)) m = 0;

	  std::pair<double, double> mERR_pair = std::make_pair(m, stderr);
	  return mERR_pair;
}

