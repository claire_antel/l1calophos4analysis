#include <iostream>
#include <iomanip>
#include <vector>
#include <map>
#include <sstream>
#include <string>
#include <fstream>
//#include "TH1.h"
		
#ifndef __CINT__
#include "TString.h"
#include "TF1.h"
#include "TFile.h"
#include "TKey.h"
#include "TObject.h"
#include "TProfile.h"
#include "TH1D.h"
#endif
//gROOT->Reset();
//gStyle->SetOptStat(0);


void smoothP4Scans(const char* fileName){ 
 
    //TH1D *smooth = 0; 
  
    TFile* mainf = new TFile(fileName);
    TFile* fout = new TFile("$HOME/20.1.3.3./Phos4analysis/output/smoothP4pulses.root", "RECREATE");
    mainf->cd("Hists");
    //TFile* f = gDirectory->GetFile();
    std::cout << "Input file: " << fileName << std::endl;
    //     TCanvas* kannwas = new TCanvas("can","can",700,500);    
            
    gDirectory->Print();
    TIter next( gDirectory->GetListOfKeys() );

    TKey* key;
    while( (key=(TKey*) next() )){
        std::cout << "key is... " << key << std::endl;
	printf("key: %s points to an object of class: %s at %dn",key->GetName(),key->GetClassName(),key->GetSeekKey());
	TObject *o = (TH1D*) key->ReadObj()->Clone(TString::Format("%s", key->GetName()));
        std::cout << "o is of class.. "<<o->ClassName()<< std::endl;
        std::cout << "object title of o.. "<<o->GetTitle()<< std::endl;
	if(!o->InheritsFrom("TH1")) continue; //only look at histos
        std::cout << "o is from TH1...continuing.. "<< std::endl;
	TH1D *smooth = (TH1D*)o;
	std::string histName = key->GetName();
        std::cout << "histname is... " << histName << std::endl;
	std::string title = smooth->GetTitle();
        std::cout << "title... " << title << std::endl;
        std::cout << "number of bins in smooth.. " << smooth->GetNbinsX() << std::endl;
	//TH1D* smooth = new TH1D(histName.c_str(), title.c_str(), 375, 0, 375);
	for(int i = 1; i < smooth->GetNbinsX()-2; i++){
	  double binC0 = smooth->GetBinContent(i-1); 
	  double binC1 = smooth->GetBinContent(i); 
	  double diff1 = fabs(binC1-binC0);
	  if(diff1 > 8 ){ // checking for sudden jumps
	  	double binC2 = smooth->GetBinContent(i+2);
	  	double diff2 = fabs(binC1-binC2);
		if(diff2 > 10 ){
			double slope = (binC2-binC0)/(3.);
			double corr = binC0+slope;
			smooth->SetBinContent(i, corr);
		}
	  }
	}
	fout->cd();
	smooth->Write();
	//delete o;
	delete smooth;
    }    
    mainf->Close();
//    fout->Write();
    fout->Close();
    
}

void smoothP4Scans(){
    smoothP4Scans("$HOME/20.1.3.3./Phos4analysis/output/recon_P4pulses.root");
}

