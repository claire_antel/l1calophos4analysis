#ifndef FITFUNCTIONS_h
#define FITFUNCTIONS_h

//Fitfunction for a parabola
double Fit_Pa(double *x, double *par);
//Fitfunction for a Parabola-Parabola
double Fit_PP(double *x, double *par);
//FitFunction for a Gauss
double Fit_G(double *x, double *par);
//Fitfunction for a Gauss-Gauss
double Fit_GG(double *x, double *par); 
//Fitfunction for a Gauss-Gauss -> reference fit for all CaloDivisions except HEC
double Fit_GL(double *, double *);
//Fitfunction for a Gauss-Landau, considering the undershoot on the Landau side
double Fit_GLu(double *, double *);
//Fitfunction for a double Landau -> reference fit for HEC
double Fit_LL(double *, double *);
//Fitfunction for a double Landau (with undershoot on right side)
double Fit_LLu(double *, double *);

#endif
