#ifndef FitFunc_h
#define FitFunc_h

#include <vector>
#include <TH1D.h>
#include <TF1.h>
#include <math.h>
#include <iostream>
#include <TFile.h>
#include <string>
#include <cstdio>
#include <TCanvas.h>
#include <TLine.h>

enum CaloPartEnum
  {
    LArFCALEM = 0, LArEMEC, LArOverlap, LArEMB, LArFCALHAD2, LArFCALHAD3,
    LArHEC, TileEB, TileLB, undef // FCALEM = 0, EMEC = 1, Overlap = 2, EMB = 3, FCALHAD2 = 4, FCALHAD3 = 5, HEC = 6, TileEB = 7, TileLB = 8 
  };

class FitFunc
{
	public:
		FitFunc();
		virtual ~FitFunc();
		double* set_parameter( int crate_number );
		double* set_parameter_us( int crate_number );
		double* set_parameter_usF( int crate_number );
		double Fit_GLu(double *x, double *par);
		double Fit_GLuF(double *x, double *par);
		double Fit_LLu(double *x, double *par);
		double Fit_LLuF(double *x, double *par);
		std::vector<int> fill_tvector( int length = 5 );
		std::vector<int> fill_vector( double *par, int length = 5 );
		std::vector<int> fill_vector_glu( double *par, int length = 5 );
		std::vector<int> fill_vector_llu( double *par, int length = 5 );
		TH1D* fill_histo( std::vector<int> tval, std::vector<int> yval, std::string hname ="hff" );
		std::vector<int> read_histo( TH1* hist );
		int do_gof( std::vector<int> yvalues, int tpeak, int crate_number );
		int do_gof_glu( std::vector<int> yvalues, int tpeak, double* parameter );
		int do_gof_llu( std::vector<int> yvalues, int tpeak, double* parameter );
		//double do_gof( int ymidbin, double tmax, double amp );
		void set_histo_color( TH1* hist, double gof );
		double do_simple_gof( int goflong );
		bool do_slice_fit_us(std::vector<int> yvalues, CaloPartEnum part, int &tmax, int &amp, double &gof, std::string hname = "hff" );
		TF1* setup_fit(CaloPartEnum part, double* par, int floedge, int fupedge);
		int get_gof(std::vector<int> yvalues, int tpeak, double* parameter, CaloPartEnum part);
		void do_slice_fit_llu(std::vector<int> yvalues, int crate_number, int &tmax, int &amp, double &gof, std::string hname = "hff" );
		void do_slice_fit_gluF(std::vector<int> yvalues, int crate_number, int &tmax, int &amp, double &gof, std::string hname = "hff" );
		void do_slice_fit_lluF(std::vector<int> yvalues, int crate_number, int &tmax, int &amp, double &gof, std::string hname = "hff" );
		double* return_parameters();
//		void do_slice_fit_us_1( std::vector<int> yvalues, int crate_number, int &tmax, int &amp, double &gof, std::string hname = "hff" );	
		TH1D* return_histogram(const char* histname);
		//void do_canvas( int tmax, int amp, std::string cname = "cff" );
		double round( double in, int prec = 0 );
		CaloPartEnum GetDetectorPart(double etaIn, double layer);
	private:
		TH1D* fithist;
		TF1* func;
		TCanvas* m_canvas;
		TLine* m_line;
		int m_tmax;
		int m_amp;
		double* m_para;
		bool m_iscanvas;
};

#endif


