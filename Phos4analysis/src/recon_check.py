# check reconstructed pulses and fit.
# output fitted pulse, chi square, sigma values, comparison to run I

from ROOT import *
#gROOT.LoadMacro("TTxml.cxx")

#run = raw_input('look at run: ')

part = raw_input('look at part: ')
TTid = raw_input('look at TT tower: ')
TTid2 = raw_input('look at second TT tower: ')

#run1='264603'#'264505'#'266851'#'264603'
#run2='264604'
#run3='174155'#'175951' #'174155'
#run4='177385'#'176209' #'177385'

if part in ["7", "8"]:
	run1='264505'
	run3="272641"
	f1 = TFile('/afs/cern.ch/work/c/cantel/private/output/'+run1+'/recon_P4pulses.root')#TFile('/afs/cern.ch/work/c/cantel/private/output/'+run1+'/recon_P4pulses.root')#TFile('/afs/cern.ch/work/c/cantel/private/output/'+run1+'/GLufit_hists.root')
	f3 = TFile('/afs/cern.ch/work/c/cantel/private/output/'+run3+'/recon_P4pulses.root')#TFile('/afs/cern.ch/work/c/cantel/private/output/'+run2+'/recon_P4pulses.root')#TFile('/afs/cern.ch/work/c/cantel/private/output/'+run2+'/GLufit_hists.root')
	#f3 = TFile('/afs/cern.ch/work/c/cantel/private/output/'+run1+'/recon_P4pulses.root')#TFile('/afs/cern.ch/work/c/cantel/private/output/'+run1+'/recon_P4pulses.root')#TFile('/afs/cern.ch/work/c/cantel/private/output/'+run1+'/GLufit_hists.root')
	#f4 = TFile('/afs/cern.ch/work/c/cantel/private/output/'+run2+'/recon_P4pulses.root')#TFile('/afs/cern.ch/work/c/cantel/private/output/'+run2+'/recon_P4pulses.root')#TFile('/afs/cern.ch/work/c/cantel/private/output/'+run2+'/GLufit_hists.root')
	key="reconpulse_part"+part+"_id"+TTid
	key2="reconpulse_part"+part+"_id"+TTid2
else:
	#run1='264603'#'264505'#'266851'#'264603'
	run1='273450'
	run2='273449'
	run3='264604'
	run4='264603'
	#run1="269939"
	#run2="269942"
	#run3="269939"
	#run4="269942"
	#run3='266851'
	#run4='266854'
	#run3='174155'
	#run4='177385'
	#run2='266851'
	f1 = TFile('/afs/cern.ch/work/c/cantel/private/output/'+run1+'/recon_P4pulses.root')#TFile('/afs/cern.ch/work/c/cantel/private/output/'+run2+'/recon_P4pulses.root')#TFile('/afs/cern.ch/work/c/cantel/private/output/'+run2+'/GLufit_hists.root')
	f2 = TFile('/afs/cern.ch/work/c/cantel/private/output/'+run2+'/recon_P4pulses.root')#TFile('/afs/cern.ch/work/c/cantel/private/output/'+run2+'/recon_P4pulses.root')#TFile('/afs/cern.ch/work/c/cantel/private/output/'+run2+'/GLufit_hists.root')
#	f3 = TFile('/afs/cern.ch/work/c/cantel/private/output/'+run3+'/recon_P4pulses.root')#TFile('/afs/cern.ch/work/c/cantel/private/output/'+run2+'/recon_P4pulses.root')#TFile('/afs/cern.ch/work/c/cantel/private/output/'+run2+'/GLufit_hists.root')
#	f4 = TFile('/afs/cern.ch/work/c/cantel/private/output/'+run4+'/recon_P4pulses.root')#TFile('/afs/cern.ch/work/c/cantel/private/output/'+run2+'/recon_P4pulses.root')#TFile('/afs/cern.ch/work/c/cantel/private/output/'+run2+'/GLufit_hists.root')
	f3 = TFile('/afs/cern.ch/work/c/cantel/private/output/'+run3+'/recon_P4pulses.root')
	f4 = TFile('/afs/cern.ch/work/c/cantel/private/output/'+run4+'/recon_P4pulses.root')
	#f3 = TFile('/afs/cern.ch/work/c/cantel/private/output/runI/Phos4scan'+run3+'_pedestalcorrected.root')
	#f4 = TFile('/afs/cern.ch/work/c/cantel/private/output/runI/Phos4scan'+run4+'_pedestalcorrected.root')
	key="reconpulse_part"+part+"_id"+TTid
	key2="reconpulse_part"+part+"_id"+TTid2
	#key2="Histcorr0x"+TTid2

c2 = TCanvas('c2', 'c2', 1024, 800)
c2.Divide(2,2)

#d("Hists")

print "looking at histogram "+key

p1exists= False
p2exists= False
p3exists= False
p4exists= False

if 'f1' in locals():
	f1.cd("Hists")
	#key="reconpulse_part3_id"+TTid
	if key in gDirectory.GetListOfKeys():
		p1exists = True
		pulse1=gDirectory.Get(key)
		pulse1.SetMarkerColor(kBlue+2)
		pulse1.SetMarkerStyle(7)
		pulse1.SetTitle("pulse TT0x"+TTid+" from run "+run1)
		c2.cd(1)
		pulse1.Draw("C")

#f2.cd("Hists")
if 'f2' in locals():
	f2.cd("Hists")
	if key in gDirectory.GetListOfKeys():
		p2exists = True
		pulse2=gDirectory.Get(key)
		c2.cd(2)
		pulse2.SetMarkerColor(kAzure+2)
		pulse2.SetMarkerStyle(7)
		pulse2.SetTitle("pulse TT0x"+TTid+" from run "+run2)
		pulse2.Draw()

if 'f3' in locals():
	#f3.cd()
	f3.cd("Hists")
	if key2 in gDirectory.GetListOfKeys():
		p3exists = True
		pulse3=gDirectory.Get(key2)
		c2.cd(3)
		pulse3.SetMarkerColor(kRed+2)
		pulse3.SetMarkerStyle(7)
		pulse3.SetTitle("pulse TT0x"+TTid2+" from run "+run3)
		pulse3.Draw()

if 'f4' in locals():
	#f4.cd()
	f4.cd("Hists")
	if key2 in gDirectory.GetListOfKeys():
		p4exists = True
		pulse4=gDirectory.Get(key2)
		c2.cd(4)
		pulse4.SetMarkerColor(kGreen+2)
		pulse4.SetMarkerStyle(7)
		pulse4.SetTitle("pulse TT0x"+TTid2+" from run "+run4)
		pulse4.Draw()
		raw_input("press ENTER to continue...")

c2.WaitPrimitive()



#c.Divide(2,2)

#d("Hists")

if (p1exists == True and p3exists == True):
	shiftdown=pulse1.GetBinContent(5)
	for bin in range (0, pulse1.GetNbinsX()+1):
		pulse1.SetBinContent(bin, pulse1.GetBinContent(bin)-shiftdown)
	pulse1.SetStats(0)
	pulse1.SetTitle("comparing pulses TT0x"+TTid+" and TT0x"+TTid2+" (part "+part+")")
	#pulse1.GetXaxis().SetRangeUser(50,350)
#	pulse1.GetXaxis().SetTitle('time [ns]')
#	pulse1.GetYaxis().SetTitle('ADC counts')

	shiftdown=pulse3.GetBinContent(5)
	shift=pulse3.GetMaximumBin()-pulse1.GetMaximumBin()+3
	temp=pulse3.Clone()
	for bin in range (0, pulse3.GetNbinsX()):
		temp.SetBinContent(bin, pulse3.GetBinContent(bin+shift)-shiftdown)
	temp.Scale(pulse1.GetMaximum()/temp.GetMaximum())
	pulse3=temp
	pulse3.SetStats(0)


	c = TCanvas('c', 'c', 1024, 800)
	c.cd()
	pulse1.Draw()
	pulse3.Draw("SAME")
	raw_input("press ENTER to continue...")
	#c.WaitPrimitive()





		

