
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../reconstructp4.h"

DECLARE_ALGORITHM_FACTORY( reconstructp4 )



DECLARE_FACTORY_ENTRIES( Phos4analysis ) 
{
  DECLARE_ALGORITHM( reconstructp4 );
}
