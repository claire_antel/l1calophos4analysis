# echo "cleanup Phos4analysis Phos4analysis-00-00-00 in /afs/cern.ch/user/c/cantel/20.1.5.1/atlas-cantel"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc48-opt/20.1.5/CMT/v1r25p20140131
endif
source ${CMTROOT}/mgr/setup.csh
set cmtPhos4analysistempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if $status != 0 then
  set cmtPhos4analysistempfile=/tmp/cmt.$$
endif
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=Phos4analysis -version=Phos4analysis-00-00-00 -path=/afs/cern.ch/user/c/cantel/20.1.5.1/atlas-cantel  $* >${cmtPhos4analysistempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=Phos4analysis -version=Phos4analysis-00-00-00 -path=/afs/cern.ch/user/c/cantel/20.1.5.1/atlas-cantel  $* >${cmtPhos4analysistempfile}"
  set cmtcleanupstatus=2
  /bin/rm -f ${cmtPhos4analysistempfile}
  unset cmtPhos4analysistempfile
  exit $cmtcleanupstatus
endif
set cmtcleanupstatus=0
source ${cmtPhos4analysistempfile}
if ( $status != 0 ) then
  set cmtcleanupstatus=2
endif
/bin/rm -f ${cmtPhos4analysistempfile}
unset cmtPhos4analysistempfile
exit $cmtcleanupstatus

