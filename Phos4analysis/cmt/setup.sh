# echo "setup Phos4analysis Phos4analysis-00-00-00 in /afs/cern.ch/user/c/cantel/20.1.5.1/atlas-cantel"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc48-opt/20.1.5/CMT/v1r25p20140131; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtPhos4analysistempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmtPhos4analysistempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe setup -sh -pack=Phos4analysis -version=Phos4analysis-00-00-00 -path=/afs/cern.ch/user/c/cantel/20.1.5.1/atlas-cantel  -no_cleanup $* >${cmtPhos4analysistempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe setup -sh -pack=Phos4analysis -version=Phos4analysis-00-00-00 -path=/afs/cern.ch/user/c/cantel/20.1.5.1/atlas-cantel  -no_cleanup $* >${cmtPhos4analysistempfile}"
  cmtsetupstatus=2
  /bin/rm -f ${cmtPhos4analysistempfile}
  unset cmtPhos4analysistempfile
  return $cmtsetupstatus
fi
cmtsetupstatus=0
. ${cmtPhos4analysistempfile}
if test $? != 0 ; then
  cmtsetupstatus=2
fi
/bin/rm -f ${cmtPhos4analysistempfile}
unset cmtPhos4analysistempfile
return $cmtsetupstatus

